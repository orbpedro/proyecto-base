package pe.orbelite.apps.proyecto.di.modulo;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pe.orbelite.apps.proyecto.bd.repositorios.DaoMaster;
import pe.orbelite.apps.proyecto.bd.repositorios.DaoSession;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.bd.repositorios.implementaciones.PeticionWebRepositorioImpl;


@Module
public class GreenDaoModulo {

    public static final String NOMBRE_BD = "Proyecto";

    @Singleton
    @Provides
    DaoSession daoSession(Context context) {
        return new DaoMaster(new DaoMaster.DevOpenHelper(context, NOMBRE_BD).getWritableDb()).newSession();
    }

    @Singleton
    @Provides
    PeticionWebRepositorio peticionWebRepositorio(DaoSession daoSession) {
        return new PeticionWebRepositorioImpl(daoSession);
    }

}
