package pe.orbelite.apps.proyecto.mvp.presenters;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginRespuesta;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import pe.orbelite.apps.proyecto.api.YoPresido.servicios.UsuarioServicio;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.SharedPreferencesRepositorio;
import pe.orbelite.apps.proyecto.mvp.contratos.LoginContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.LoginContrato.Presenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginPresenter implements Presenter {

    private WeakReference<LoginContrato.View> view;
    private Retrofit retrofit;
    private SharedPreferencesRepositorio sharedPreferencesRepositorio;

    @Inject
    public LoginPresenter(Retrofit retrofitProyecto, SharedPreferencesRepositorio sharedPreferencesRepositorio){
        this.retrofit = retrofitProyecto;
        this.sharedPreferencesRepositorio = sharedPreferencesRepositorio;
    }

    @Override
    public void validarLogueo(LoginPeticion loginPeticion) {

        Call<LoginRespuesta> call = retrofit.create(UsuarioServicio.class).loguear(loginPeticion);
        call.enqueue(new Callback<LoginRespuesta>() {
            @Override
            public void onResponse(Call<LoginRespuesta> call, Response<LoginRespuesta> response) {
                //Log.i("LOGIN-RES","Res: "+response.body());
                switch (response.code()) {
                    case 200:
                        //getView().mostrarLog(response.body().get(0).getTitulo());
                        //Log.i("LOGIN-RES","Res 200");
                        //Log.i("LOGIN-RES","Res: "+response.body().toString());
                        //Log.i("LOGIN-RES","Res: "+response.body().getToken());
                        manejarLoginCorrecto(response.body());
                        break;
                    case 400:
                        //Log.i("LOGIN-RES","Res 400");
                        Gson gson = new GsonBuilder().create();
                        /*Esta sección muestra datos de la respuesta 400*/
                        PeticionResult mError=new PeticionResult();
                        try {
                            mError= gson.fromJson(response.errorBody().string(),PeticionResult.class);
                            //Log.i("LOGIN-ERR","ErrCod "+mError.getErrorCod());
                            //Log.i("LOGIN-ERR","ErrLisTam "+mError.getErrorLis().size());
                            //Log.i("LOGIN-ERR","ErrLisMen "+mError.getErrorLis().toString());
                        } catch (IOException e) {
                            // handle failure to read error
                        }
                        /*
                        Este método muestra los parámetros que se enviaron en el call
                        BufferedSink bf = new Buffer();
                        try { call.request().body().writeTo(bf);
                        } catch (IOException e) { e.printStackTrace(); }
                        Log.i("LOGIN-PAR",bf.buffer().readUtf8().toString());
                        */
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<LoginRespuesta> call, Throwable t) {
                //Log.e("LOGIN-RES","Error en el request.");

            }
        });
    }

    private void manejarLoginCorrecto(LoginRespuesta respuesta) {
        sharedPreferencesRepositorio.guardarTokenAcceso(respuesta.getToken());
        view.get().logueoOk(respuesta);
    }

    @Override
    public void attachView(LoginContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
