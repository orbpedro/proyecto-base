package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubirImagenRespuesta {

    @SerializedName("id")
    @Expose
    private String archivoId;

    public String getId() {
        return archivoId;
    }

    public void setId(String archivoId) {
        this.archivoId = archivoId;
    }
}
