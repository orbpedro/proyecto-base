package pe.orbelite.apps.proyecto;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import pe.orbelite.apps.proyecto.di.componentes.DaggerProyectoAppComponente;
import pe.orbelite.apps.proyecto.di.componentes.ProyectoAppComponente;
import pe.orbelite.apps.proyecto.di.modulo.ProyectoAppModulo;

public class ProyectoApp extends Application {

    public static final String NOMBRE_PREFERENCIA = "pe.orbelite.apps.infraxionpro";

    public static final String LLAVE_TOKEN_ACCESO = "LLAVE_TOKEN_ACCESO";

    public  static final String LLAVE_GOOGLE_API = "AIzaSyBz_bOUVhYtmPMNMG5qMP95_QvZJBGOPxI";

    public  static  final String DOMAIN = "http://161.132.121.44/InfraxionIntegracion/";
    //public  static final String DOMAIN = "http://161.132.121.44/InfraxionServices/";

    private ProyectoAppComponente proyectoAppComponente;

    private static GoogleAnalytics analytics;
    private static Tracker tracker;

    @Override
    public void onCreate() {
        super.onCreate();
        proyectoAppComponente = DaggerProyectoAppComponente.builder().proyectoAppModulo(new ProyectoAppModulo(this)).build();
    }

    public ProyectoAppComponente getComponent() {
        return proyectoAppComponente;
    }

    public static ProyectoApp from(@NonNull Context context) {
        return (ProyectoApp) context.getApplicationContext();
    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (tracker == null) {
            tracker = analytics.newTracker(R.xml.global_tracker);
        }

        return tracker;
    }

}
