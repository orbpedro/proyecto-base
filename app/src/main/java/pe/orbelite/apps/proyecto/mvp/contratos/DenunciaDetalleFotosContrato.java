package pe.orbelite.apps.proyecto.mvp.contratos;

import java.io.File;

import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface DenunciaDetalleFotosContrato {

    interface View extends BaseView {

        void verficarCamara();
        void abrirCamara();
        void mostrarRecorte(File file);
        void subirCorteOk(String archivoId);
        void subirCorteKo();

    }

    interface Presenter extends BasePresenter<DenunciaDetalleFotosContrato.View> {

        void subirCorte(File file);
    }

}
