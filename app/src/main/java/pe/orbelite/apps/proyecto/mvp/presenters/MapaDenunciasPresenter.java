package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.DenunciaService;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaDenunciasContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaDenunciasContrato.Presenter;
import pe.orbelite.apps.proyecto.utilitarios.FormatoUtilitario;
import pe.orbelite.apps.proyecto.utilitarios.PeticionWebUtilitario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MapaDenunciasPresenter implements Presenter {

    private WeakReference<MapaDenunciasContrato.View> view;
    private Retrofit retrofit;
    private PeticionWebRepositorio peticionWebRepositorio;

    @Inject
    public MapaDenunciasPresenter(Retrofit retrofit,PeticionWebRepositorio peticionWebRepositorio) {
        this.retrofit = retrofit;
        this.peticionWebRepositorio = peticionWebRepositorio;
    }

    @Override
    public void getDenuncias() {
        String json = PeticionWebUtilitario.obtenerJsonDePeticionWeb(peticionWebRepositorio, "denuncia/obtener_denuncias_cercanas");
        List<Denuncia> denuncias = FormatoUtilitario.listaDeJson(json, Denuncia[].class);
        view.get().mostrarMensajeEnDebug("De caché "+denuncias.size());

        if (denuncias != null) {
            view.get().mostrarDenuncias(denuncias);
            return;
        }

        return;
    }

    @Override
    public void putDenunciaGestionar(String id) {
        final String myId=id;
        DenunciaPeticion peticion = new DenunciaPeticion();
        peticion.setId(id);

        Call<PeticionResult> call = retrofit.create(DenunciaService.class).putDenunciaGestionar(peticion);
        call.enqueue(new Callback<PeticionResult>() {
            @Override
            public void onResponse(Call<PeticionResult> call, Response<PeticionResult> response) {
                view.get().mostrarMensajeEnDebug("WS res: "+response.code());

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        view.get().mostrarMensajeEnDebug("Se marcó la denuncia como gestionada correctamente.");
                        view.get().onDialogVerClick(myId);
                        break;
                    default:
                        view.get().mostrarMensajeEnDebug("Hubo un problema al actualizar el estado, intente nuevamente por favor.");
                        view.get().mostrarMensajeEnDebug("Código de error: "+response.body().getErrorCod());
                        view.get().mostrarMensajeEnDebug("Errores"+response.body().getErrorLis());
                        break;
                }
            }

            @Override
            public void onFailure(Call<PeticionResult> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en peticion" + t.getMessage());
            }
        });
    }

    @Override
    public void attachView(MapaDenunciasContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
