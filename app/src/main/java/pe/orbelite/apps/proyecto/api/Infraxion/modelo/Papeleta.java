package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Papeleta {

    @SerializedName("placa")
    @Expose
    private String placa;

    @SerializedName("falta")
    @Expose
    private String falta;

    @SerializedName("estado")
    @Expose
    private String estado;

    @SerializedName("concepto")
    @Expose
    private String concepto;

    @SerializedName("licencia")
    @Expose
    private String licencia;

    @SerializedName("documento")
    @Expose
    private String documento;

    @SerializedName("importe")
    @Expose
    private String importe;

    @SerializedName("descuento")
    @Expose
    private String descuento;

    @SerializedName("porcentajeDescuento")
    @Expose
    private String porcentajeDescuento;

    @SerializedName("gastoCosto")
    @Expose
    private String gastoCosto;

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("fechaEmision")
    @Expose
    private String fechaEmision;

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFalta() {
        return falta;
    }

    public void setFalta(String falta) {
        this.falta = falta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(String porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public String getGastoCosto() {
        return gastoCosto;
    }

    public void setGastoCosto(String gastoCosto) {
        this.gastoCosto = gastoCosto;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
}
