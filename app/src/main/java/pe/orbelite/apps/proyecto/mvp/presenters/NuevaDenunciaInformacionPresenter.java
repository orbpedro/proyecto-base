package pe.orbelite.apps.proyecto.mvp.presenters;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.api.Google.servicios.GoogleMapService;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.gps.Localizacion;
import pe.orbelite.apps.proyecto.gps.LocalizacionCallBack;
import pe.orbelite.apps.proyecto.gps.Localizador;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaInformacionContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaInformacionContrato.Presenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NuevaDenunciaInformacionPresenter implements Presenter, LocalizacionCallBack {

    private WeakReference<NuevaDenunciaInformacionContrato.View> view;
    private Retrofit retrofit;
    private Retrofit googleMapsApi;
    private Localizador localizador;
    private PeticionWebRepositorio peticionWebRepositorio;

    public NuevaDenunciaInformacionPresenter(Retrofit retrofit,
                                             Localizador localizador,
                                             Retrofit googleMapsApi,
                                             PeticionWebRepositorio peticionWebRepositorio){
        this.retrofit = retrofit;
        this.localizador = localizador;
        this.googleMapsApi = googleMapsApi;
        this.localizador.setCallback(this);
        this.peticionWebRepositorio = peticionWebRepositorio;
    }

    @Override
    public void iniciarGeoLocalizacion() {
        //view.get().mostrarMensajeEnDebug("Localizacion iniciando.");
        if (!view.get().tieneAccesoGeo()) {
            return;
        }

        localizador.cancelarLocalizacion();
        localizador.iniciarLocalizacion();
        this.view.get().mostrarBarraEjecucion();
        //view.get().mostrarMensajeEnDebug("Localizacion iniciada.");
}

    @Override
    public void localizacionTerminada(Localizacion localizacion) {
        //view.get().mostrarMensajeEnDebug("Localizacion terminada.");
        this.view.get().ocultarBarraEjecucion();

        if (!localizacion.isSuceso() || localizacion.getLocation() == null) {
            this.view.get().errorAlConseguirDatosGeo();
            return;
        }

        //view.get().mostrarMensajeEnDebug("Localizacion encontrada : " + localizacion.getLocation().getLatitude() + "," + localizacion.getLocation().getLongitude());

        this.view.get().mostrarBarraEjecucion();
        view.get().mostrarRutaMapa(localizacion.getLocation().getLatitude(), localizacion.getLocation().getLongitude());
        String latitudLongitud = localizacion.getLocation().getLatitude() + "," + localizacion.getLocation().getLongitude();

        view.get().setCargoData(true);

        Call<Object> call = googleMapsApi.create(GoogleMapService.class).obtenerDatosCoordenada(latitudLongitud, ProyectoApp.LLAVE_GOOGLE_API);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                //view.get().mostrarMensajeEnDebug("RES "+response.code());

                if (view.get() == null) {
                    return;
                }

                view.get().ocultarBarraEjecucion();

                switch (response.code()) {
                    case 200:
                        String json = new Gson().toJson(response.body());
                        String direccion = obtenerDireccion(json);

                        view.get().mostrarLugar(direccion);
                        //view.get().mostrarMensajeEnDebug("La direccion es:" + direccion);
                        break;
                    default:
                        view.get().errorAlConseguirLugar();
                        break;
                }

                //view.get().mostrarMensajeEnDebug(response.raw().request().url() + "");
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                //view.get().mostrarMensajeEnDebug("FAIL");
            }
        });

    }

    private String obtenerDireccion(String jsonString) {
        String direccion = "";

        try {

            view.get().mostrarMensajeEnDebug(jsonString);

            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray resultadosJson = jsonObject.getJSONArray("results");
            if (resultadosJson.length() > 0) {
                JSONObject addressJson = resultadosJson.getJSONObject(0);

                String ubicacion = addressJson.getString("formatted_address");

                String codigoPostal = "no codigo";

                JSONArray componentesJson = addressJson
                        .getJSONArray("address_components");
                for (int i = 0; i < componentesJson.length(); i++) {
                    JSONObject componenteJson = componentesJson
                            .getJSONObject(i);
                    JSONArray typesJson = componenteJson.getJSONArray("types");
                    for (int j = 0; j < typesJson.length(); j++) {
                        String tipo = typesJson.getString(j);
                        // hacerLogDebug(tipo);
                        if ("postal_code".equalsIgnoreCase(tipo)) {
                            codigoPostal = componenteJson
                                    .getString("short_name");
                        }
                    }
                }

                if (ubicacion != null) {
                    ubicacion = ubicacion.replace(codigoPostal, "");
                }

                direccion = ubicacion;
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return direccion;
    }

    @Override
    public void attachView(NuevaDenunciaInformacionContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view.clear();
    }
}
