package pe.orbelite.apps.proyecto.mvp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.ErrorGeneral;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginRespuesta;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.contratos.LoginContrato;

public class LoginFragment extends BaseFragment implements LoginContrato.View {

    @Inject
    LoginContrato.Presenter presenter;

    private View root;
    //private Toolbar toolbar;
    private EditText txtUsuario;
    private EditText txtContrasena;
    private Button btnIngresar;
    private TextView tvCrear;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_login, container, false);
        configurarControles();

        return root;
    }

    private void configurarControles() {
        //toolbar=(Toolbar)getActivity().findViewById(R.id.toolbar);
        //toolbar.setTitle(R.string.login_titulo);

        txtUsuario = root.findViewById(R.id.etUsuario);
        txtContrasena = root.findViewById(R.id.etContrasena);

        btnIngresar=root.findViewById(R.id.btnIngresar);
        //tvCrear=root.findViewById(R.id.tvCrear);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean err=false;
                if(TextUtils.isEmpty(txtUsuario.getText())){
                    txtUsuario.setError( "Debe llenar este campo." );
                    err=true;
                }

                if(TextUtils.isEmpty(txtContrasena.getText())){
                    txtContrasena.setError( "Debe llenar este campo." );
                    err=true;
                }

                if(err==true) return;
                else loguear();
            }
        });

        /*
        tvCrear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //fragmentCrear();
            }
        });
        */
    }

    /*
    private void fragmentCrear(){
        SigninFragment sf=new SigninFragment();
        cambiarFragmento(sf);
    }
    */

    @Override
    public void loguear() {
        LoginPeticion loginPeticion = new LoginPeticion();
        loginPeticion.setUsuario(txtUsuario.getText().toString());
        loginPeticion.setContrasena(txtContrasena.getText().toString());

        presenter.validarLogueo(loginPeticion);
        //presenter.validar("dzurita","demo$$2018*");
    }

    @Override
    public void logueoOk(LoginRespuesta respuesta) {
        ocultarPantallaBloqueo();
        mostrarMensajeEnDebug(respuesta.getToken());

        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));

        getActivity().finish();
    }

    @Override
    public void logueoKo(String respuesta) {
        ocultarPantallaBloqueo();
        ErrorGeneral errorGeneral = obtenerDeJson(respuesta);
        String mensaje = "No se pudo realizar el ingreso";
        if (errorGeneral.getErrores() != null && errorGeneral.getErrores().size() > 0) {
            mensaje = errorGeneral.getErrores().get(0);
        }

        mostrarMensajeSimple(mensaje);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
