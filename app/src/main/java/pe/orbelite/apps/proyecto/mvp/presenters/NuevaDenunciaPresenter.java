package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;

import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.gps.Localizacion;
import pe.orbelite.apps.proyecto.gps.LocalizacionCallBack;
import pe.orbelite.apps.proyecto.gps.Localizador;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaContrato.Presenter;
import retrofit2.Retrofit;

public class NuevaDenunciaPresenter implements Presenter {

    private WeakReference<NuevaDenunciaContrato.View> view;
    private Retrofit retrofit;

    public NuevaDenunciaPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void miFuncion() {

    }

    @Override
    public void attachView(NuevaDenunciaContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view.clear();
    }
}
