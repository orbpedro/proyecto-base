package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Soat {

    @SerializedName("placa")
    @Expose
    private String placa;

    @SerializedName("estado")
    @Expose
    private String estado;

    @SerializedName("inicio")
    @Expose
    private String inicio;

    @SerializedName("fin")
    @Expose
    private String fin;

    @SerializedName("compania")
    @Expose
    private String compania;

    @SerializedName("costo")
    @Expose
    private double costo;

    @SerializedName("claseVehiculo")
    @Expose
    private String claseVehiculo;

    @SerializedName("usoVehiculo")
    @Expose
    private String usoVehiculo;

    @SerializedName("tipoCertificado")
    @Expose
    private String tipoCertificado;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public String getClaseVehiculo() {
        return claseVehiculo;
    }

    public void setClaseVehiculo(String claseVehiculo) {
        this.claseVehiculo = claseVehiculo;
    }

    public String getUsoVehiculo() {
        return usoVehiculo;
    }

    public void setUsoVehiculo(String usoVehiculo) {
        this.usoVehiculo = usoVehiculo;
    }

    public String getTipoCertificado() {
        return tipoCertificado;
    }

    public void setTipoCertificado(String tipoCertificado) {
        this.tipoCertificado = tipoCertificado;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
}
