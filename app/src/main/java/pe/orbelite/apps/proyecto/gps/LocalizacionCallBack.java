package pe.orbelite.apps.proyecto.gps;

public interface LocalizacionCallBack {
    void localizacionTerminada(Localizacion localizacion);
}
