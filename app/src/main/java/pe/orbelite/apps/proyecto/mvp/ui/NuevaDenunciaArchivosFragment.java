package pe.orbelite.apps.proyecto.mvp.ui;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaArchivosContrato;
import pe.orbelite.apps.proyecto.utilitarios.PermisosUtilitario;

public class NuevaDenunciaArchivosFragment extends BaseFragment implements NuevaDenunciaArchivosContrato.View {

    @Inject
    NuevaDenunciaArchivosContrato.Presenter presenter;

    FrameLayout contenedorPantalla = null;
    private View root;
    private Button btnAgregarFoto;

    private Uri fotoURI;
    private Uri fotoCortadaURI;
    private File fotoArchivo;
    private File fotoCortadaArchivo;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;

    public NuevaDenunciaArchivosFragment() {
        // Required empty public constructor
    }

    public static NuevaDenunciaArchivosFragment newInstance() {
        NuevaDenunciaArchivosFragment fragment = new NuevaDenunciaArchivosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_nueva_denuncia_archivos, container, false);
        configurarControles();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        btnAgregarFoto = root.findViewById(R.id.btnAgregar);
        btnAgregarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirCamara();
            }
        });


    }

    @Override
    public void verficarCamara() {
        btnAgregarFoto.setEnabled(false);

        Context context = getActivity();
        PackageManager packageManager = context.getPackageManager();
        if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
            mostrarMensajeSimple("Este equipo no tiene disponible una cámara.");
            return;
        }

        if(!tieneAccesoCamaraVerificados()){
            return ;
        }
    }

    @Override
    public void abrirCamara() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            //Create a file to store the image
            try {
                fotoArchivo = crearArchivoImagenVacio();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (fotoArchivo != null) {
                fotoURI = FileProvider.getUriForFile(getContext(),
                        "pe.orbelite.apps.proyecto.fileprovider",
                        fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        fotoURI);

                startActivityForResult(intent,CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        }
    }

    @Override
    public void mostrarRecorte(File file) {

    }

    @Override
    public void subirCorteOk(String archivoId) {

    }

    @Override
    public void subirCorteKo() {

    }

    String ArchivoImagenVacioRuta;
    private File crearArchivoImagenVacio() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        ArchivoImagenVacioRuta = image.getAbsolutePath();
        mostrarMensajeEnDebug("Ruta imagen vacía "+ArchivoImagenVacioRuta);
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mostrarMensajeEnDebug("Verificando permisos...");
        switch (requestCode) {
            case PermisosUtilitario.PEDIR_PERMISO_CAMARA: {
                if (PermisosUtilitario.aceptoTodosLosPermisos(grantResults)) {
                    mostrarMensajeEnDebug("Iniciando Cámara");
                    abrirCamara();
                } else {
                    mostrarMensajeSimple("No puede realizar ninguna acción si no habilita los permisos solicitados");
                    setCargoData(true);
                    ocultarBarraProgresoIndeterminado(contenedorPantalla);
                }
                return;
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                CropImage.activity(fotoURI).start(getContext(), this);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            //mostrarMensajeEnDebug("Entro a tomar la imagen cortada");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    Bitmap croppedPhoto = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),
                            resultUri);
                    //imageView.setImageBitmap(croppedPhoto);

                    ContextWrapper cw = new ContextWrapper(getContext());
                    File directory = cw.getDir("denuncia", Context.MODE_PRIVATE);
                    if (!directory.exists()) {
                        directory.mkdir();
                        //mostrarMensajeEnDebug("Carpeta Wallet creada.");
                    }

                    fotoCortadaArchivo=new File(directory, "thumbnail.png");


                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(fotoCortadaArchivo);
                        //croppedPhoto.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        croppedPhoto.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.close();
                        mostrarMensajeEnDebug("Proceso de guardado completo.");

                        mostrarPantallaBloqueo("Guardando corte,\n espere un momento por favor...");
                        presenter.subirCorte(fotoCortadaArchivo);
                    } catch (Exception e) {
                        mostrarMensajeEnDebug("SAVE_IMAGE " + e.getMessage() + e);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                mostrarMensajeEnDebug(error.toString());
            }
        }
    }
}
