package pe.orbelite.apps.proyecto.utilitarios;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

public class PermisosUtilitario {

    public  static final int PEDIR_PERMISO_GEOLOCALIZACION = 45678;
    public  static final int PEDIR_PERMISO_LLAMADA = 45656;
    public  static final int PEDIR_PERMISO_CAMARA= 45634;


    public static boolean aceptoTodosLosPermisos(int[] grantResults) {

        if (grantResults.length == 0) {
            return false;
        }

        for (int i = 0; i < grantResults.length; i++) {
            int resultado = grantResults[i];
            if (resultado != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }


        return true;
    }

    public static boolean verificarListaDePermisosParaUtilizarGeoLocalizacion(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion < android.os.Build.VERSION_CODES.M) {
            return true;
        }


        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        return true;
    }


    public static boolean verificarListaDePermisosParaUtilizarLlamada(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion < android.os.Build.VERSION_CODES.M) {
            return true;
        }


        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        return true;
    }

    public static boolean verificarListaDePermisosParaUtilizarCamara(final Context context) {
        /*
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        */

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        return true;
    }

}
