package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaDenunciasContrato;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.DenunciaListaItem;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.DenunciaListaItemAdapter;
import pe.orbelite.apps.proyecto.mvp.ui.controles.RecyclerItemClickListener;
import pe.orbelite.apps.proyecto.mvp.ui.dialogos.DetalleMapaDenunciaDialogFragment;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;


public class MapaDenunciasFragment extends BaseFragment implements MapaDenunciasContrato.View,
        DetalleMapaDenunciaDialogFragment.OnDialogClickListener {

    @Inject
    MapaDenunciasContrato.Presenter presenter;

    FrameLayout contenedorPantalla = null;
    private View root;
    RecyclerView listDatos = null;
    LinearLayoutManager manager = null;

    private boolean iniciated=false;
    List<Denuncia> listDenuncias = new ArrayList<>();

    public MapaDenunciasFragment() {
        // Required empty public constructor
    }


    public static MapaDenunciasFragment newInstance() {
        MapaDenunciasFragment fragment = new MapaDenunciasFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
        mostrarMensajeEnDebug("Create...");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_mapa_denuncias, container, false);
        configurarControles();
        //mostrarMensajeEnDebug("CreateV...");
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mostrarMensajeEnDebug("Resuming...");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            mostrarMensajeEnDebug("Visible");
            if(iniciated==false){
                presenter.getDenuncias();
                //iniciated=true;
            }
        }else{
            mostrarMensajeEnDebug("Oculto");
        }
    }

    private void configurarControles() {
        listDatos = root.findViewById(R.id.listDatos);
        listDatos.setHasFixedSize(true);
        manager = new LinearLayoutManager(getActivity());

        listDatos.setLayoutManager(manager);
        listDatos.setAdapter(new DenunciaListaItemAdapter());
    }

    private void verDenunciaPopup(DenunciaListaItem denuncia){
        mostrarMensajeEnDebug("Denuncia "+denuncia.getTitulo());
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        android.support.v4.app.Fragment prev = fragmentManager.findFragmentByTag("DIALOGO_MOSTRAR_DENUNCIA");
        if (prev != null) {
            ft.remove(prev);
        }

        ft.addToBackStack(null);

        DetalleMapaDenunciaDialogFragment fragment = DetalleMapaDenunciaDialogFragment.newInstance(denuncia.getId(),
                denuncia.getTitulo(),
                denuncia.getUrl(),
                denuncia.getFecha(),
                denuncia.getDescripcion(),
                denuncia.getEstado(),
                denuncia.getDireccion(),
                this);


        fragment.show(ft, "DIALOGO_MOSTRAR_SERVICIO");

        prev = null;
        ft = null;
        fragmentManager = null;

    }

    @Override
    public void onDialogGestionarClick(String id) {
        presenter.putDenunciaGestionar(id);
    }

    @Override
    public void onDialogVerClick(String id) {
        final String myId = id;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cambiarFragment(DenunciaDetalleFragment.newInstance(myId));
            }
        });
    }

    @Override
    public void mostrarDenuncias(List<Denuncia> denuncias) {
        if(denuncias.size()>0){

            List<DenunciaListaItem> lista = new ArrayList<>();
            for(Denuncia denuncia:denuncias){
                DenunciaListaItem item = new DenunciaListaItem();
                item.setId(denuncia.getId());
                item.setTitulo("Título falso");
                item.setEstado(denuncia.getEstado());
                item.setUrl(denuncia.getRutaFoto());
                item.setFecha(denuncia.getFecha());
                item.setDescripcion(denuncia.getDescripcion());
                item.setDireccion(denuncia.getDireccion());

                lista.add(item);
            }

            ((DenunciaListaItemAdapter) listDatos.getAdapter()).agregarDatos(lista);
            listDatos.getAdapter().notifyDataSetChanged();
            listDatos.getAdapter().notifyItemRangeChanged(0, listDatos.getAdapter().getItemCount());

            listDatos.addOnItemTouchListener(
                    new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            final DenunciaListaItem item = ((DenunciaListaItemAdapter) listDatos.getAdapter()).getDatos().get(position);
                            verDenunciaPopup(item);
                            //generarEventoPopup();
                        }
                    })
            );
        }else{
            mostrarMensajeSimple("No hay denuncias para mostrar.");
        }

        //this.iniciated=true;
    }
}
