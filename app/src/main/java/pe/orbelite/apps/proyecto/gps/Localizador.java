package pe.orbelite.apps.proyecto.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;

public class Localizador {

    private long INTERVALO_ACTUALIZACION = 5 * 1000;
    private long INTERVALO_RAPIDO = 1000;

    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private Context context;
    private LocalizacionCallBack callback;
    private LocationCallback locationCallback;

    public Localizador(Context context) {
        this.context = context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setCallback(LocalizacionCallBack callback) {
        this.callback = callback;
    }

    public void iniciarLocalizacion() {

        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(INTERVALO_ACTUALIZACION);
        locationRequest.setFastestInterval(INTERVALO_RAPIDO);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this.context);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.context);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocalizacionEncontrada(locationResult);
            }
        };

        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.myLooper());


    }

    public  void cancelarLocalizacion(){
        removerActualizacion();
    }

    private void onLocalizacionEncontrada(LocationResult locationResult) {

        Location location = locationResult.getLastLocation();

        Localizacion localizacion = new Localizacion();
        localizacion.setSuceso(location != null);
        localizacion.setLocation(location);
        localizacion.setCodigo(location != null ? Localizacion.SUCESO : Localizacion.NO_SE_PUDO_ENCONTRAR_COORDENADA);
        localizacion.setMensaje(location != null ? "suceso" : "No se pudo encontrar coordenada");

        removerActualizacion();


        callback.localizacionTerminada(localizacion);
    }

    private void removerActualizacion() {
        if (fusedLocationClient == null || locationCallback == null) {
            return;
        }

        fusedLocationClient.removeLocationUpdates(this.locationCallback);
    }

}
