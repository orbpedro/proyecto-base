package pe.orbelite.apps.proyecto.mvp.ui.viewModel;

public class VehiculoViewModel {

    private String placa;
    private String marca;
    private String modelo;
    private String color;
    private String estado;
    private SoatViewModel soat;


    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public SoatViewModel getSoat() {
        return soat;
    }

    public void setSoat(SoatViewModel soat) {
        this.soat = soat;
    }
}
