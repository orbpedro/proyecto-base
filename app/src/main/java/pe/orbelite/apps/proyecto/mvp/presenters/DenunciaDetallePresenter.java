package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Foto;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.DenunciaService;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleContrato.Presenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DenunciaDetallePresenter implements Presenter {

    private WeakReference<DenunciaDetalleContrato.View> view;
    private Retrofit retrofit;

    private Denuncia miDenuncia;

    @Inject
    public DenunciaDetallePresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void buscarDenunciaDetalle(String denunciaId) {
        //view.get().mostrarMensajeEnDebug(denunciaId);
        DenunciaPeticion peticion = new DenunciaPeticion();
        peticion.setId(denunciaId);

        Call<Denuncia> call = retrofit.create(DenunciaService.class).getDenunciaDetalle(peticion);
        call.enqueue(new Callback<Denuncia>() {
            @Override
            public void onResponse(Call<Denuncia> call, Response<Denuncia> response) {
                view.get().mostrarMensajeEnDebug("WS res: "+response.code());

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        miDenuncia=response.body();
                        view.get().mostrarDenunciaDetalle(miDenuncia);
                        break;
                    default:
                        view.get().noDenunciaDetalle();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Denuncia> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en peticion" + t.getMessage());
            }
        });
    }

    @Override
    public void guardarDenuncia(Denuncia denuncia) {
        Call<PeticionResult> call = retrofit.create(DenunciaService.class).putDenunciaGenerar(denuncia);
        call.enqueue(new Callback<PeticionResult>() {
            @Override
            public void onResponse(Call<PeticionResult> call, Response<PeticionResult> response) {
                view.get().mostrarMensajeEnDebug("WS res: "+response.code());

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        view.get().mostrarMensajeEnDebug("Se generó la denuncia correctamente.");
                        view.get().goDenunciaImprimir();
                        break;
                    default:
                        view.get().noDenunciaDetalle();
                        break;
                }
            }

            @Override
            public void onFailure(Call<PeticionResult> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en peticion" + t.getMessage());
            }
        });
    }

    @Override
    public void attachView(DenunciaDetalleContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
