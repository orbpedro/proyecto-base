package pe.orbelite.apps.proyecto.api.Infraxion.servicios;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Vehiculo;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.VehiculoPeticion;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VehiculoService {

    @POST("api/usuario/vehiculos_por_placa")
    Call<Vehiculo> obtenerPorPlaca(@Body VehiculoPeticion peticion);

}
