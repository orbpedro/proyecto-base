package pe.orbelite.apps.proyecto.mvp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.utilitarios.ImagenesAutoUtilitario;

public class MiVehiculoItemAdapter extends RecyclerView.Adapter<MiVehiculoItemAdapter.ViewHolder> {

    private List<MiVehiculoItem> datos;


    public MiVehiculoItemAdapter() {
        datos = new ArrayList<>();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_vehiculo, parent, false);
        // set the view's size, margins, paddings and fragment_home parameters
        MiVehiculoItemAdapter.ViewHolder vh = new MiVehiculoItemAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MiVehiculoItem item = datos.get(position);
        holder.tvTitulo.setText(item.getTitulo() != null ? item.getTitulo() : "");
        holder.tvSoatEstado.setText(item.getSoatEstado() != null ? "SOAT " + item.getSoatEstado() : "Sin soat");
        holder.tvPapeletas.setText("Papeletas Asociadas: " + item.getTotalPapeletas());


        String descripcion = "";
        if (item.getMarca() != null) {
            descripcion += item.getMarca() + " - ";
        }

        if (item.getModelo() != null) {
            descripcion += item.getModelo() + ", ";
        }

        if (item.getColor() != null) {
            descripcion += item.getColor();
        }

        holder.tvDescripcion.setText(descripcion);
        if ("vencido".equalsIgnoreCase(item.getSoatEstado()) || item.getSoatEstado() == null || item.getTotalPapeletas() > 0) {
            holder.tvTitulo.setTextColor(ContextCompat.getColor(holder.tvTitulo.getContext(), R.color.item_vehiculo_no_sano));
        }

        holder.ivImagen.setImageResource(ImagenesAutoUtilitario.iconIdDeClaseVehiculo(item.getSoatClaseVehiculo()));
        holder.ivSoat.setImageResource(ImagenesAutoUtilitario.iconoSoat(item.getSoatCompania()));

    }

    @Override
    public int getItemCount() {
        return datos.size();
    }


    public void agregarDatos(List<MiVehiculoItem> masDatos) {
        datos.clear();
        datos.addAll(masDatos);
    }

    public List<MiVehiculoItem> getDatos() {
        return datos;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitulo;
        private TextView tvSoatEstado;
        private TextView tvPapeletas;
        private TextView tvDescripcion;
        private ImageView ivImagen;
        private ImageView ivSoat;


        public ViewHolder(View v) {
            super(v);
            tvTitulo = v.findViewById(R.id.tvTitulo);
            tvSoatEstado = v.findViewById(R.id.tvSoatEstado);
            tvPapeletas = v.findViewById(R.id.tvPapeletas);
            tvDescripcion = v.findViewById(R.id.tvDescripcion);
            ivImagen = v.findViewById(R.id.ivImagen);
            ivSoat = v.findViewById(R.id.ivSoat);
        }
    }
}
