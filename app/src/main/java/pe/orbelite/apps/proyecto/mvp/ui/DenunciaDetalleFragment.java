package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Foto;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Infraccion;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleContrato;

public class DenunciaDetalleFragment extends PantallaFragment implements DenunciaDetalleContrato.View,
DenunciaDetalleAdicionalesFragment.OnAdicionalesListener,
DenunciaDetalleFotosFragment.OnDenunciaFotosListener,
DenunciaDetalleInfraxionesFragment.OnDenunciaInfraccionesListener{

    private static final String ARG_DENUNCIA_ID = "ARG_DENUNCIA_ID";

    @Inject
    DenunciaDetalleContrato.Presenter presenter;

    private DenunciaDetalleInformacionFragment fragmentInformacion;
    private DenunciaDetalleInfraxionesFragment fragmentInfracciones;
    private DenunciaDetalleAdicionalesFragment fragmentAdicionales;
    private DenunciaDetalleFotosFragment fragmentFotos;

    private FrameLayout contenedorPantalla;
    private View root;
    private TextView tvTitulo;
    private TextView tvFecha;
    private TextView tvDireccion;
    private ImageView ivFoto;
    private TabLayout tabs;

    private ViewPager viewPager;
    private Menu myMenu;

    private String denunciaId;
    private boolean menuIniciado=false;
    private List<Infraccion> infracciones = new ArrayList<Infraccion>();
    private List<Foto> fotos = new ArrayList<Foto>();
    private String adicional;
    private boolean iniciado=false;

    private static final int MENU_GENERAR = 1111115;
    private static final int MENU_IMPRIMIR = 1111116;
    private static final int MENU_VER = 1111117;

    private Denuncia denuncia;

    public DenunciaDetalleFragment() {
        // Required empty public constructor
    }

    public static DenunciaDetalleFragment newInstance(String denunciaId) {
        DenunciaDetalleFragment fragment = new DenunciaDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DENUNCIA_ID, denunciaId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            denunciaId = getArguments().getString(ARG_DENUNCIA_ID);
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mostrarMensajeEnDebug("Resuming...");
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.titulo_detalle_denuncia);
        cargarDatos();

        if(iniciado==false){
            mostrarBarraProgresoIndeterminado(contenedorPantalla);
            mostrarPantallaBloqueo("Buscando denuncia...");
            mostrarMensajeEnDebug("Cargando data a partir del ID de denuncia.");
            presenter.buscarDenunciaDetalle(denunciaId);
            iniciado=true;
        }else{
            menuIniciado=false;
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            mostrarDenunciaDetalle(denuncia);
                        }
                    }, 1000);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_denuncia_detalle, container, false);
        configurarControles();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        menuIniciado=false;
        this.myMenu = menu;

        this.myMenu.add(0, MENU_GENERAR, 3, getString(R.string.generar)).setVisible(false)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        this.myMenu.add(0, MENU_IMPRIMIR, 4, getString(R.string.imprimir)).setVisible(false)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        this.myMenu.add(0, MENU_VER, 5, getString(R.string.ver)).setVisible(false)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);


        if(this.denuncia!=null
                && menuIniciado==false){
            switch (this.denuncia.getEstado()) {
                case "1":
                    this.myMenu.findItem(MENU_GENERAR).setVisible(true);
                    break;
                case "2":
                    this.myMenu.findItem(MENU_GENERAR).setVisible(true);
                    break;
                case "3":
                    this.myMenu.findItem(MENU_GENERAR).setVisible(true);
                    break;
                case "4":
                    this.myMenu.findItem(MENU_IMPRIMIR).setVisible(true);
                    break;
                case "5":
                    this.myMenu.findItem(MENU_IMPRIMIR).setVisible(true);
                    break;
                case "7":
                    this.myMenu.findItem(MENU_VER).setVisible(true);
                    break;
            }
        }

        menuIniciado=true;
        mostrarMensajeEnDebug("Opc creadas "+this.myMenu.size());
        super.onCreateOptionsMenu(this.myMenu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case MENU_GENERAR:
                generarDenuncia(infracciones, fotos, adicional);
                return true;

            case MENU_IMPRIMIR:
                goDenunciaImprimir();
                return true;

            case MENU_VER:
                goDenunciaImprimir();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        ivFoto = root.findViewById(R.id.ivFoto);
        tvTitulo = root.findViewById(R.id.tvTitulo);
        tvFecha = root.findViewById(R.id.tvFecha);
        tvDireccion = root.findViewById(R.id.tvDireccion);
        tabs = root.findViewById(R.id.result_tabs);

        viewPager = root.findViewById(R.id.viewpager);
    }

    @Override
    public void mostrarDenunciaDetalle(Denuncia denuncia) {
        this.denuncia=denuncia;

        tvTitulo.setText("Denuncia Titulo");
        tvFecha.setText(this.denuncia.getFecha());
        tvDireccion.setText(this.denuncia.getDireccion());
        Picasso.get().load(this.denuncia.getRutaFoto()).into(ivFoto);
        //mostrarMensajeEnDebug("Estado: "+this.denuncia.getEstado());
        ocultarPantallaBloqueo();

        fragmentInformacion = DenunciaDetalleInformacionFragment.newInstance(this.denuncia.getPlaca(), this.denuncia.getDescripcion());

        infracciones = this.denuncia.getInfracciones();
        fragmentInfracciones = DenunciaDetalleInfraxionesFragment.newInstance((Serializable) this.denuncia.getInfracciones(), this);

        fotos = this.denuncia.getFotos();
        fragmentFotos = DenunciaDetalleFotosFragment.newInstance((Serializable) this.denuncia.getFotos(), this);

        mostrarMensajeEnDebug("Menu: "+this.myMenu.size());

        if(menuIniciado==true) {
            switch (this.denuncia.getEstado()) {
                case "1":
                    this.myMenu.findItem(MENU_GENERAR).setVisible(true);
                    break;
                case "2":
                    this.myMenu.findItem(MENU_GENERAR).setVisible(true);
                    break;
                case "3":
                    this.myMenu.findItem(MENU_GENERAR).setVisible(true);
                    break;
                case "4":
                    this.myMenu.findItem(MENU_IMPRIMIR).setVisible(true);
                    break;
                case "5":
                    this.myMenu.findItem(MENU_IMPRIMIR).setVisible(true);
                    break;
                case "7":
                    this.myMenu.findItem(MENU_VER).setVisible(true);
                    break;
            }
        }

        setupViewPager(viewPager);
        tabs.setupWithViewPager(viewPager);
        ocultarBarraProgresoIndeterminado(contenedorPantalla);
    }

    @Override
    public void noDenunciaDetalle() {
        tvTitulo.setText("No se encontró la denuncia");
        tvFecha.setText("--/--/----");
        tvDireccion.setText("----------");

        irAtras();
        mostrarMensajeSimple("No se encontró la denuncia");
    }

    private void cargarDatos() {

    }

    @Override
    public void generarDenuncia(List<Infraccion> infracciones, List<Foto> fotos, String nota) {
        this.denuncia.setInfracciones(infracciones);
        this.denuncia.setFotos(fotos);
        this.denuncia.setNota(nota);

        presenter.guardarDenuncia(denuncia);
    }

    @Override
    public void goDenunciaImprimir() {
        //Fragment myFragment=this;
        //removerFragmentDelBack(myFragment);
        cambiarFragment(DenunciaDetalleImprimirFragment.newInstance(denunciaId));
    }

    private void setupViewPager(ViewPager viewPager) {

        DenunciaDetalleAdapter adapter = new DenunciaDetalleAdapter(getChildFragmentManager());
        adapter.addFragment(fragmentInformacion, "Informacion");
        adapter.addFragment(fragmentInfracciones,"Infracciones");
        adapter.addFragment(fragmentFotos,"Fotos");
        adapter.addFragment(DenunciaDetalleAdicionalesFragment.newInstance(this.denuncia.getNota(),this), "Adicionales");

        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);
        viewPager.setAdapter(adapter);
    }

    static class DenunciaDetalleAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public DenunciaDetalleAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public Parcelable saveState()
        {
            return null;
        }
    }

    @Override
    public void onDestroy() {
        mostrarMensajeEnDebug("destruyendo");
        super.onDestroy();
    }





    @Override
    public Void getFoto(String fotoId) {
        if(fotoId != null){
            mostrarMensajeEnDebug(fotoId);
            Foto nuevaFoto = new Foto();
            nuevaFoto.setId(fotoId);

            this.fotos.add(nuevaFoto);
            nuevaFoto = null;
            System.gc();

            mostrarMensajeEnDebug("fotos "+this.fotos.size());
        }

        return null;
    }

    @Override
    public Void getInfracciones(List<Infraccion> infracciones) {
        this.infracciones=infracciones;
        //Log.i("INFRAXIONES","Hay "+this.infracciones.size());
        for(Infraccion i:infracciones){
            Log.i("Infracción",i.getCategoria());
        }
        return null;
    }

    @Override
    public Void getNota(String adicional) {
        this.adicional=adicional;
        return null;
    }
}
