package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiVehiculoItem;
import pe.orbelite.apps.proyecto.mvp.ui.viewModel.CarritoItemViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VehiculoSoatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehiculoSoatFragment extends BaseFragment {

    private static final String ARG_SOAT_ESTADO = "ARG_SOAT_ESTADO";
    private static final String ARG_SOAT_INICIO = "ARG_SOAT_INICIO";
    private static final String ARG_SOAT_FIN = "ARG_SOAT_FIN";
    private static final String ARG_SOAT_COMPANIA = "ARG_SOAT_COMPANIA";
    private static final String ARG_SOAT_COSTO = "ARG_SOAT_COSTO";
    private static final String ARG_SOAT_CLASE_VEHICULO = "ARG_SOAT_CLASE_VEHICULO";
    private static final String ARG_SOAT_USO_VEHICULO = "ARG_SOAT_USO_VEHICULO";
    private static final String ARG_SOAT_TIPO_CERTIFICADO = "ARG_SOAT_TIPO_CERTIFICADO";
    private static final String ARG_PLACA = "ARG_PLACA";

    private String soatEstado;
    private String soatInicio;
    private String soatFin;
    private String soatCompania;
    private double soatCosto;
    private String soatClaseVehiculo;
    private String soatUsoVehiculo;
    private String soatTipoCertificado;
    private String placa;

    private View root;
    private TextView tvCompania;
    private TextView tvInicio;
    private TextView tvFin;
    private TextView tvEstado;
    private TextView tvClaseVehiculo;
    private TextView tvUsoVehiculo;
    private Button btnPagar;

    public VehiculoSoatFragment() {
        // Required empty public constructor
    }


    public static VehiculoSoatFragment newInstance(MiVehiculoItem item) {
        VehiculoSoatFragment fragment = new VehiculoSoatFragment();
        Bundle args = new Bundle();

        args.putString(ARG_SOAT_ESTADO, item.getEstado());
        args.putString(ARG_SOAT_INICIO, item.getSoatInicio());
        args.putString(ARG_SOAT_FIN, item.getSoatFin());
        args.putString(ARG_SOAT_COMPANIA, item.getSoatCompania());
        args.putDouble(ARG_SOAT_COSTO, item.getSoatCosto());
        args.putString(ARG_SOAT_CLASE_VEHICULO, item.getSoatUsoVehiculo());
        args.putString(ARG_SOAT_USO_VEHICULO, item.getSoatUsoVehiculo());
        args.putString(ARG_SOAT_TIPO_CERTIFICADO, item.getSoatTipoCertificado());
        args.putString(ARG_PLACA, item.getPlaca());

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            soatEstado = getArguments().getString(ARG_SOAT_ESTADO);
            soatInicio = getArguments().getString(ARG_SOAT_INICIO);
            soatFin = getArguments().getString(ARG_SOAT_FIN);
            soatCompania = getArguments().getString(ARG_SOAT_COMPANIA);
            soatCosto = getArguments().getDouble(ARG_SOAT_COSTO);

            soatClaseVehiculo = getArguments().getString(ARG_SOAT_CLASE_VEHICULO);
            soatUsoVehiculo = getArguments().getString(ARG_SOAT_USO_VEHICULO);
            soatTipoCertificado = getArguments().getString(ARG_SOAT_TIPO_CERTIFICADO);
            placa = getArguments().getString(ARG_PLACA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment

        root = inflater.inflate(R.layout.fragment_vehiculo_soat, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarDatos();
    }

    private void configurarControles() {
        tvCompania = root.findViewById(R.id.tvCompania);
        tvEstado = root.findViewById(R.id.tvEstado);
        tvInicio = root.findViewById(R.id.tvInicio);
        tvFin = root.findViewById(R.id.tvFin);
        tvClaseVehiculo = root.findViewById(R.id.tvClaseVehiculo);
        tvUsoVehiculo = root.findViewById(R.id.tvUsoVehiculo);
        btnPagar = root.findViewById(R.id.btnPagar);

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pagarSoat();
            }
        });

    }

    private void cargarDatos() {
        tvCompania.setText(soatCompania != null ? soatCompania : "-");
        tvInicio.setText(soatInicio != null ? soatInicio : "-");
        tvFin.setText(soatFin != null ? soatFin : "-");
        tvEstado.setText(soatEstado != null ? soatEstado : "-");
        tvUsoVehiculo.setText(soatUsoVehiculo != null ? soatUsoVehiculo : "-");
        tvClaseVehiculo.setText(soatClaseVehiculo != null ? soatClaseVehiculo : "-");

    }

    private void pagarSoat() {

        List<CarritoItemViewModel> listaCarrito = new ArrayList<>();


        CarritoItemViewModel item = new CarritoItemViewModel();
        String descripcion = "Soat para placa " + placa;


        item.setCantidad(1);
        item.setDescripcion(descripcion);
        item.setTotalPagar(soatCosto);
        listaCarrito.add(item);

        //cambiarFragment(CarritoSimpleFragment.newInstance((Serializable) listaCarrito));

    }

}
