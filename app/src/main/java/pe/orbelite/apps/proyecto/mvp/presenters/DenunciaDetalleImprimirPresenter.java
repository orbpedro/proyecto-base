package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.DenunciaService;
import pe.orbelite.apps.proyecto.mvp.contratos.BusquedaPlacaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleFotosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleImprimirContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleImprimirContrato.Presenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DenunciaDetalleImprimirPresenter implements Presenter {

    private WeakReference<DenunciaDetalleImprimirContrato.View> view;
    private Retrofit retrofit;

    private Denuncia miDenuncia;

    public DenunciaDetalleImprimirPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void getDenuncia(String denunciaId) {
        DenunciaPeticion peticion = new DenunciaPeticion();
        peticion.setId(denunciaId);

        Call<Denuncia> call = retrofit.create(DenunciaService.class).getDenunciaDetalle(peticion);
        call.enqueue(new Callback<Denuncia>() {
            @Override
            public void onResponse(Call<Denuncia> call, Response<Denuncia> response) {
                //view.get().mostrarMensajeEnDebug("WS res: "+response.code());

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        miDenuncia=response.body();
                        view.get().mostrarDenunciaDatos(miDenuncia);
                        break;
                    default:
                        //view.get().noDenunciaDetalle();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Denuncia> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en peticion" + t.getMessage());
            }
        });
    }

    @Override
    public void putDenunciaImprimir() {
        DenunciaPeticion peticion = new DenunciaPeticion();
        peticion.setId(miDenuncia.getId());

        Call<PeticionResult> call = retrofit.create(DenunciaService.class).putDenunciaImprimir(peticion);
        call.enqueue(new Callback<PeticionResult>() {
            @Override
            public void onResponse(Call<PeticionResult> call, Response<PeticionResult> response) {
                //view.get().mostrarMensajeEnDebug("WS res: "+response.code());

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        view.get().impresoOk();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<PeticionResult> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                //view.get().mostrarMensajeEnDebug("Error en peticion" + t.getMessage());
                view.get().impresoKo();
            }
        });
    }

    @Override
    public void attachView(DenunciaDetalleImprimirContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view.clear();
    }
}
