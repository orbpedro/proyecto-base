package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginPeticion {
    @SerializedName("username")
    @Expose
    private String usuario;

    @SerializedName("password")
    @Expose
    private String contrasena;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
