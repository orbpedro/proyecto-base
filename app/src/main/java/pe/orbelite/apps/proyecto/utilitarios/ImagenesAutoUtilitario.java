package pe.orbelite.apps.proyecto.utilitarios;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

import pe.orbelite.apps.proyecto.R;

public class ImagenesAutoUtilitario {


    public static int iconIdDeClaseVehiculo(String claseVehiculo) {

        if (TextUtils.isEmpty(claseVehiculo)) {
            return R.drawable.vehiculo_general;
        }

        Map<String, Integer> iconosAutos = new HashMap<>();
        iconosAutos.put("AUTOMOVIL", R.drawable.vehiculo_general);
        iconosAutos.put("MOTO / VEH. MENOR", R.drawable.vehiculo_moto);
        iconosAutos.put("OMNIBUS", R.drawable.vehiculo_omnibus);
        iconosAutos.put("CAMION", R.drawable.vehiculo_camion);
        iconosAutos.put("CAMIONETA PANEL", R.drawable.vehiculo_camioneta);
        iconosAutos.put("CAMIONETA PICK UP", R.drawable.vehiculo_pick_up);
        iconosAutos.put("CAMIONETA RURAL", R.drawable.vehiculo_camioneta);
        iconosAutos.put("MICROBUS", R.drawable.vehiculo_omnibus);
        iconosAutos.put("FURGONETA", R.drawable.vehiculo_furgoneta);
        iconosAutos.put("AMBULANCIA", R.drawable.vehiculo_camioneta);
        iconosAutos.put("EQUIPOS Y MAQUINARIA", R.drawable.vehiculo_camion);
        iconosAutos.put("GRUA", R.drawable.vehiculo_camion);
        iconosAutos.put("REMOLQUE", R.drawable.vehiculo_camion);
        iconosAutos.put("STATION WAGON", R.drawable.vehiculo_station_wagon);
        iconosAutos.put("TRACTO CAMION/REMOLCADOR", R.drawable.vehiculo_camion);
        iconosAutos.put("TRIMOVIL", R.drawable.vehiculo_moto);


        if (iconosAutos.containsKey(claseVehiculo.trim())) {
            return iconosAutos.get(claseVehiculo.trim());
        }


        return R.drawable.vehiculo_general;
    }


    public static int iconoSoat(String compania) {

        if (TextUtils.isEmpty(compania)) {
            return R.drawable.icono_defecto;
        }

        Map<String, Integer> iconosSoat = new HashMap<>();
        iconosSoat.put("bnp paribas cardif", R.drawable.icono_paribas);
        iconosSoat.put("interseguro", R.drawable.icono_interseguro);
        iconosSoat.put("la positiva", R.drawable.icono_la_positiva);
        iconosSoat.put("mapfre peru", R.drawable.icono_mapfre);
        iconosSoat.put("pacifico seguros", R.drawable.icono_pacifico);
        iconosSoat.put("protecta", R.drawable.icono_protecta);
        iconosSoat.put("rimac seguros", R.drawable.icono_rimac);



        if (iconosSoat.containsKey(compania.trim().toLowerCase())) {
            return iconosSoat.get(compania.trim().toLowerCase());
        }


        return R.drawable.icono_defecto;
    }

}
