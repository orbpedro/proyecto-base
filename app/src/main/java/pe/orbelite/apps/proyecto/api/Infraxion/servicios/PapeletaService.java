package pe.orbelite.apps.proyecto.api.Infraxion.servicios;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Papeleta;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PapeletasPeticion;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PapeletaService {

    @POST("api/papeletas/obtener_papeletas_por_vehiculo")
    Call<List<Papeleta>> listarPapeletas(@Body PapeletasPeticion peticion);
}
