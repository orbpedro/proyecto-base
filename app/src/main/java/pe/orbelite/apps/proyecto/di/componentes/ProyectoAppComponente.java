package pe.orbelite.apps.proyecto.di.componentes;

import javax.inject.Singleton;

import dagger.Component;
import pe.orbelite.apps.proyecto.di.modulo.ApiModule;
import pe.orbelite.apps.proyecto.di.modulo.GreenDaoModulo;
import pe.orbelite.apps.proyecto.di.modulo.LocalizacionModule;
import pe.orbelite.apps.proyecto.di.modulo.PreferencesModule;
import pe.orbelite.apps.proyecto.di.modulo.ProyectoAppModulo;
import pe.orbelite.apps.proyecto.mvp.ui.BusquedaPlacaFragment;
import pe.orbelite.apps.proyecto.mvp.ui.DenunciaDetalleFotosFragment;
import pe.orbelite.apps.proyecto.mvp.ui.DenunciaDetalleFragment;
import pe.orbelite.apps.proyecto.mvp.ui.DenunciaDetalleImprimirFragment;
import pe.orbelite.apps.proyecto.mvp.ui.HomeFragment;
import pe.orbelite.apps.proyecto.mvp.ui.LoginFragment;
import pe.orbelite.apps.proyecto.mvp.ui.MapaDenunciasFragment;
import pe.orbelite.apps.proyecto.mvp.ui.MapaMapaFragment;
import pe.orbelite.apps.proyecto.mvp.ui.MisDenunciasFragment;
import pe.orbelite.apps.proyecto.mvp.ui.NuevaDenunciaArchivosFragment;
import pe.orbelite.apps.proyecto.mvp.ui.NuevaDenunciaFragment;
import pe.orbelite.apps.proyecto.mvp.ui.NuevaDenunciaInformacionFragment;
import pe.orbelite.apps.proyecto.mvp.ui.VehiculoDetalleFragment;
import pe.orbelite.apps.proyecto.mvp.ui.VehiculoPapeletasFragment;

@Singleton
@Component(
        modules = {
                ApiModule.class,
                ProyectoAppModulo.class,
                LocalizacionModule.class,
                PreferencesModule.class,
                GreenDaoModulo.class
        }
)
public interface ProyectoAppComponente {

    void inject(LoginFragment loginFragment);
    void inject(HomeFragment homeFragment);
    //void inject(SigninFragment signinFragment);
    //void inject(RecordarContrasenaFragment recordarContrasenaFragment);
    void inject(BusquedaPlacaFragment busquedaPlacaFragment);
    void inject(VehiculoDetalleFragment vehiculoDetalleFragment);
    void inject(VehiculoPapeletasFragment vehiculoPapeletasFragment);

    void inject(MisDenunciasFragment misDenunciasFragment);
    void inject(DenunciaDetalleFragment denunciaDetalleFragment);
    void inject(DenunciaDetalleFotosFragment denunciaDetalleFotosFragment);
    void inject(MapaMapaFragment mapaMapaFragment);
    void inject(MapaDenunciasFragment mapaDenunciasFragment);
    void inject(DenunciaDetalleImprimirFragment denunciaDetalleImprimirFragment);

    void inject(NuevaDenunciaFragment nuevaDenunciaFragment);
    void inject(NuevaDenunciaInformacionFragment nuevaDenunciaInformacionFragment);
    void inject(NuevaDenunciaArchivosFragment nuevaDenunciaArchivosFragment);
}