package pe.orbelite.apps.proyecto.mvp.presenters;

import java.io.File;
import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.SubirImagenRespuesta;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.ArchivoService;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleFotosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleFotosContrato.Presenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DenunciaDetalleFotosPresenter implements Presenter {

    private WeakReference<DenunciaDetalleFotosContrato.View> view;
    private Retrofit retrofit;

    public DenunciaDetalleFotosPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void subirCorte(File file) {
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        Call<SubirImagenRespuesta> call = retrofit.create(ArchivoService.class).subirCorte(fileBody);
        call.enqueue(new Callback<SubirImagenRespuesta>() {
            @Override
            public void onResponse(Call<SubirImagenRespuesta> call, Response<SubirImagenRespuesta> response) {
                if (view.get() == null) {
                    return;
                }

                switch (response.code()) {
                    case 200:
                        view.get().subirCorteOk(response.body().getId());
                        //view.get().mostrarRecorte(null);
                        break;
                    default:
                        view.get().subirCorteKo();
                        break;
                }

            }

            @Override
            public void onFailure(Call<SubirImagenRespuesta> call, Throwable t) {
                view.get().subirCorteKo();
            }
        });
    }

    @Override
    public void attachView(DenunciaDetalleFotosContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view.clear();
    }
}
