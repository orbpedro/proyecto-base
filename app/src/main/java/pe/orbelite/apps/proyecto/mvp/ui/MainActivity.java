package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.List;

import pe.orbelite.apps.proyecto.componentes.ui.base.BaseActivity;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView nav;

    private FrameLayout mainFrame;
    private Toolbar toolbar;

    BaseFragment homeFragment = null;
    BaseFragment busquedaPlacaFragment = null;
    BaseFragment misDenunciasFragment = null;
    BaseFragment mapaDenunciasFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbarPrincipal);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.drawer_abrir, R.string.drawer_cerrar);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        cambiarFragment(getMapaInfraxion());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            alertarFragments();
            //Log.d("MainFragment","Regresando");
            super.onBackPressed();
        }
    }

    private void alertarFragments(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for(Fragment f : fragments){
            if(f != null && f instanceof BaseFragment)
                ((BaseFragment)f).onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == android.R.id.home && !isEsRaiz()) {
            onBackPressed();
            return true;
        }

        if (id == android.R.id.home && isEsRaiz()) {
            return toggle.onOptionsItemSelected(item);
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        switch (id) {
            case R.id.nav_menu_mapa:
                //item.setChecked(true);
                cambiarFragment(getMapaInfraxion());
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_menu_vehiculo:
                //item.setChecked(true);
                cambiarFragment(getBusquedaPlaca());
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_menu_denuncias:
                //item.setChecked(true);
                cambiarFragment(getMisDenuncias());
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_cerrar:
                cambiarFragment(getHome());
                break;
        }

        return true;
    }

    public BaseFragment getHome() {
        if (homeFragment == null) {
            homeFragment = HomeFragment.newInstance();
        }
        return homeFragment;
    }

    public BaseFragment getMapaInfraxion() {
        if (mapaDenunciasFragment == null) {
            mapaDenunciasFragment = MapaFragment.newInstance();
        }
        return mapaDenunciasFragment;
    }

    public BaseFragment getBusquedaPlaca() {

        if (busquedaPlacaFragment == null) {
            busquedaPlacaFragment = BusquedaPlacaFragment.newInstance();
        }
        return busquedaPlacaFragment;
    }

    public BaseFragment getMisDenuncias() {
        if (misDenunciasFragment == null) {
            misDenunciasFragment = MisDenunciasFragment.newInstance();
        }
        return misDenunciasFragment;
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
