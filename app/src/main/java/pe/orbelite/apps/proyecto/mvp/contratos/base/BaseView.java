package pe.orbelite.apps.proyecto.mvp.contratos.base;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.ErrorGeneral;

public interface BaseView {
    void mostrarMensajeEnDebug(String mensaje);

    void accionGeneralEnErrorAccion();

    public boolean isCargoData();

    public void setCargoData(boolean cargoData);
    ErrorGeneral obtenerDeJson(String json);
}
