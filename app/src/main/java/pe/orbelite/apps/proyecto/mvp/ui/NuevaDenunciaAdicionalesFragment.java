package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NuevaDenunciaAdicionalesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NuevaDenunciaAdicionalesFragment extends BaseFragment {

    private OnNuevaDenunciaAdicionalesListener listener;

    private View root;
    private EditText edtNota;
    private String nota=null;

    public NuevaDenunciaAdicionalesFragment() {
        // Required empty public constructor
    }

    public static NuevaDenunciaAdicionalesFragment newInstance(OnNuevaDenunciaAdicionalesListener listener) {
        NuevaDenunciaAdicionalesFragment fragment = new NuevaDenunciaAdicionalesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setListener(listener);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_nueva_denuncia_adicionales, container, false);
        configurarControles();
        return root;
    }

    private void configurarControles() {
        edtNota = root.findViewById(R.id.edtNota);
        edtNota.addTextChangedListener((new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                nota=edtNota.getText().toString();
                listener.getAdicionalesData(nota);
            }
        }));
    }

    public void setListener(OnNuevaDenunciaAdicionalesListener listener) {
        this.listener = listener;
    }

    public interface OnNuevaDenunciaAdicionalesListener {

        Void getAdicionalesData(String nota);

    }

}
