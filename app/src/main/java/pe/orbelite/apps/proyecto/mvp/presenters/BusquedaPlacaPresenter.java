package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Vehiculo;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.VehiculoPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.VehiculoService;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.SharedPreferencesRepositorio;
import pe.orbelite.apps.proyecto.mvp.contratos.BusquedaPlacaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.BusquedaPlacaContrato.Presenter;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiVehiculoItem;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Call;

public class BusquedaPlacaPresenter implements Presenter {

    private WeakReference<BusquedaPlacaContrato.View> view;
    private Retrofit retrofit;
    private SharedPreferencesRepositorio sharedPreferencesRepositorio;

    @Inject
    public BusquedaPlacaPresenter(Retrofit retrofitProyecto){
        this.retrofit = retrofitProyecto;
    }

    @Override
    public void buscarPlaca(String placa) {
        VehiculoPeticion peticion = new VehiculoPeticion();
        peticion.setPlaca(placa);
        Call<Vehiculo> call = retrofit.create(VehiculoService.class).obtenerPorPlaca(peticion);
        call.enqueue(new Callback<Vehiculo>() {
            @Override
            public void onResponse(Call<Vehiculo> call, Response<Vehiculo> response) {

                if (view.get() == null) {
                    return;
                }

                switch (response.code()) {
                    case 200:
                        //listarVehiculosOk(response.body());
                        view.get().busquedaResultadoOk(response.body());
                        break;
                    default:
                        view.get().mostrarMensajeEnDebug(response.raw().message());
                        //view.get().accionGeneralEnErrorAccion();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Vehiculo> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en consulta" + t.getMessage());
            }
        });
    }

    @Override
    public void attachView(BusquedaPlacaContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
