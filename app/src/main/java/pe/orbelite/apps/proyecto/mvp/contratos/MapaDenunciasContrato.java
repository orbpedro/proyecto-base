package pe.orbelite.apps.proyecto.mvp.contratos;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface MapaDenunciasContrato {

    interface View extends BaseView {
        void mostrarDenuncias(List<Denuncia> denuncias);
        void onDialogVerClick(String id);
    }

    interface Presenter extends BasePresenter<View> {
        void getDenuncias();
        void putDenunciaGestionar(String id);
    }

}
