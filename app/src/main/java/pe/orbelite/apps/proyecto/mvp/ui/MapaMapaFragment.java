package pe.orbelite.apps.proyecto.mvp.ui;


import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaMapaContrato;
import pe.orbelite.apps.proyecto.mvp.ui.controles.PicassoMarker;
import pe.orbelite.apps.proyecto.mvp.ui.dialogos.DetalleMapaDenunciaDialogFragment;
import pe.orbelite.apps.proyecto.utilitarios.PermisosUtilitario;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapaDenunciasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapaMapaFragment extends BaseFragment implements  MapaMapaContrato.View,
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        DetalleMapaDenunciaDialogFragment.OnDialogClickListener {

    @Inject
    MapaMapaContrato.Presenter presenter;

    private GoogleMap miMapa;
    private MapView mapView;

    FrameLayout contenedorPantalla = null;
    private View root;
    private FloatingActionButton btnAgregarDenuncia;

    private HashMap<String, Marker> markerMap = new HashMap<>();
    private HashMap<String, Integer> markerIndex = new HashMap<>();
    List<Denuncia> listDenuncias = new ArrayList<>();

    private double miLatitud;
    private double miLongitud;

    public MapaMapaFragment() {
        // Required empty public constructor
    }

    public static MapaMapaFragment newInstance() {
        MapaMapaFragment fragment = new MapaMapaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        //mostrarMensajeEnDebug("Fragment MapaMapa");
        root = inflater.inflate(R.layout.fragment_mapa_mapa, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.titulo_mapa_denuncias);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mapView = view.findViewById(R.id.mapa);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }


    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        btnAgregarDenuncia = root.findViewById(R.id.btnAgregarDenuncia);
        btnAgregarDenuncia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragment(NuevaDenunciaFragment.newInstance());
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!markerIndex.containsKey(marker.getId())) {
            return false;
        }

        int position = markerIndex.get(marker.getId());
        final Denuncia item = listDenuncias.get(position);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //mostrarMensajeEnDebug("ID: "+item.getId());
                verDenunciaPopup(item);
            }
        }, 500);
        return false;
    }

    private void verDenunciaPopup(Denuncia denuncia) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        android.support.v4.app.Fragment prev = fragmentManager.findFragmentByTag("DIALOGO_MOSTRAR_DENUNCIA");
        if (prev != null) {
            ft.remove(prev);
        }

        ft.addToBackStack(null);
        DetalleMapaDenunciaDialogFragment fragment = DetalleMapaDenunciaDialogFragment.newInstance(denuncia.getId(),
                "Denuncia ciudadana",
                denuncia.getRutaFoto(),
                denuncia.getFecha(),
                denuncia.getDescripcion(),
                denuncia.getEstado(),
                denuncia.getDireccion(),
                this);

        fragment.show(ft, "DIALOGO_MOSTRAR_SERVICIO");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        miMapa = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        miMapa.setOnMarkerClickListener(this);

        mostrarBarraProgresoIndeterminado(contenedorPantalla);
        presenter.iniciarGeoLocalizacion();

        cargarDatos();
    }

    @Override
    public void onDialogGestionarClick(String id) {
        presenter.putDenunciaGestionar(id);
    }

    @Override
    public void onDialogVerClick(String id) {
        final String myId = id;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cambiarFragment(DenunciaDetalleFragment.newInstance(myId));
            }
        });
    }

    private void removerPuntosAnteriores() {
        for (Iterator<Map.Entry<String, Marker>> iterator = markerMap
                .entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Marker> entry = iterator.next();
            entry.getValue().remove();
        }

        markerMap.clear();
    }

    private void cargarDatos() {
        removerPuntosAnteriores();
        //presenter.getDenunciasCercanas();
    }

    private void cargarImagenMarca(final Marker marker, String url) {
        if (url == null || url.trim().length() == 0) {
            return;
        }

        PicassoMarker picaMarker = new PicassoMarker(marker);
        Picasso.get().load(url).resize(80, 80).centerCrop().into(picaMarker);

    }

    @Override
    public boolean tieneAccesoGeo() {
        return tieneAccesoGeoVerificados();
    }

    @Override
    public void mostrarMensajeNoHayCoordenada() {
        mostrarMensajeSimple("No se pudo encontrar su localización");
        ocultarBarraProgresoIndeterminado(contenedorPantalla);
    }

    @Override
    public void moverMapaAUnLugar(Location location) {
        if (miMapa == null) {
            return;
        }

        miLatitud=location.getLatitude();
        miLongitud=location.getLongitude();

        mostrarMensajeEnDebug("Lat: "+miLatitud+", Lon: "+miLongitud);
        final CameraPosition actual = CameraPosition.builder().target(
                new LatLng(
                        miLatitud,
                        miLongitud
                )
        ).zoom(16).bearing(8).tilt(45).build();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                miMapa.moveCamera(CameraUpdateFactory.newCameraPosition(actual));
            }
        });
    }

    @Override
    public void mostrarDenunciasCercanas(List<Denuncia> denuncias) {
        ocultarBarraProgresoIndeterminado(contenedorPantalla);

        int contador = 0;
        for (Denuncia denuncia : denuncias){
            MarkerOptions markerOptions = new MarkerOptions();

            markerOptions.position(
                    new LatLng(denuncia.getLatitud(), denuncia.getLongitud()));

            Marker marker = miMapa.addMarker(markerOptions);
            String idMarker = marker.getId();
            markerMap.put(idMarker, marker);
            markerIndex.put(idMarker, contador);
            listDenuncias.add(denuncia);

            cargarImagenMarca(marker, "https://cdn0.iconfinder.com/data/icons/security-double-color-red-and-black-vol-2/52/light__emergency__police__security-128.png");

            contador++;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mostrarMensajeEnDebug("Verificando permisos...");
        switch (requestCode) {
            case PermisosUtilitario.PEDIR_PERMISO_GEOLOCALIZACION: {
                if (PermisosUtilitario.aceptoTodosLosPermisos(grantResults)) {
                    mostrarMensajeEnDebug("Iniciando GEO");
                    presenter.iniciarGeoLocalizacion();
                } else {
                    mostrarMensajeSimple("No puede realizar ninguna acción si no habilita los permisos solicitados");
                    setCargoData(true);
                    ocultarBarraProgresoIndeterminado(contenedorPantalla);
                }
                return;
            }

        }
    }
}
