package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.mvp.contratos.HomeContrato;
import retrofit2.Retrofit;

public class HomePresenter implements HomeContrato.Presenter {

    private WeakReference<HomeContrato.View> view;
    private Retrofit retrofit;
    private PeticionWebRepositorio peticionWebRepositorio;

    @Inject
    public HomePresenter(Retrofit retrofit,PeticionWebRepositorio peticionWebRepositorio) {
        this.retrofit = retrofit;
        this.peticionWebRepositorio = peticionWebRepositorio;
    }

    @Override
    public void cerrarSesion() {
        peticionWebRepositorio.eliminarTodo();
    }

    @Override
    public void attachView(HomeContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
