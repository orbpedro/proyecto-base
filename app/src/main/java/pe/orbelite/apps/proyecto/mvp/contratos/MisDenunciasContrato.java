package pe.orbelite.apps.proyecto.mvp.contratos;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Papeleta;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface MisDenunciasContrato {

    interface View extends BaseView {

        void mostrarDenuncias(List<Denuncia> denuncias);

    }

    interface Presenter extends BasePresenter<MisDenunciasContrato.View> {

        void listarDenuncias();
    }

}
