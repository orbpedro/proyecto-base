package pe.orbelite.apps.proyecto.componentes.ui.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import pe.orbelite.apps.proyecto.R;

public class BaseActivity extends AppCompatActivity {

    private boolean esRaiz;

    protected void cambiarFragment(Fragment fragment) {

        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main_frame, fragment, fragmentTag);
            ft.addToBackStack(backStateName);
            ft.commit();
        }

    }

    public MaterialDialog.Builder obtenerDialogPorDefecto() {
        return new MaterialDialog.Builder(this).cancelable(false).theme(Theme.LIGHT).positiveColorRes(R.color.negro);
    }

    public MaterialDialog.Builder obtenerDialogPorDefectoConDosBotones() {
        return new MaterialDialog.Builder(this).cancelable(false).theme(Theme.LIGHT).positiveColorRes(R.color.negro).negativeColorRes(R.color.negro);
    }

    protected void mostrarPantallaBloqueo(String mensaje) {
        ocultarPantallaBloqueo();

        FrameLayout contenedor = findViewById(R.id.contenedorActividad);
        //LinearLayout contenedor = findViewById(R.id.contenedorActividad);
        if (contenedor == null) {
            return;
        }

        RelativeLayout rlBloquearPantalla = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.view_bloquear_pantalla, null, false);
        if (rlBloquearPantalla == null) {
            return;
        }

        rlBloquearPantalla.setClickable(true);
        if (mensaje != null) {
            TextView tvVistaBloquearPantallaTexto = rlBloquearPantalla.findViewById(R.id.tvVistaBloquearPantallaTexto);
            tvVistaBloquearPantallaTexto.setText(mensaje);
        }

        contenedor.addView(rlBloquearPantalla);
    }

    protected void ocultarPantallaBloqueo() {


        RelativeLayout contenedorBloquearPantalla = findViewById(R.id.rlBloquearPantalla);
        if (contenedorBloquearPantalla == null) {
            return;
        }
        if (contenedorBloquearPantalla.getParent() == null) {
            return;
        }
        ((ViewGroup) contenedorBloquearPantalla.getParent()).removeView(contenedorBloquearPantalla);

    }

    public boolean isEsRaiz() {
        return esRaiz;
    }

    public void setEsRaiz(boolean esRaiz) {
        this.esRaiz = esRaiz;
    }

    public void forzarLimpiarMemoria() {
        System.gc();
        Runtime.getRuntime().gc();
    }
}
