package pe.orbelite.apps.proyecto.api.Infraxion.servicios;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginRespuesta;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.MiUsuario;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Peticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginPeticion;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UsuarioService {

    @POST("api/usuario/obtener")
    Call<MiUsuario> obtenerMiUsuario(@Body Peticion peticion);

    @POST("api/usuario/login")
    Call<LoginRespuesta> login(@Body LoginPeticion peticion);

    /*
    @POST("api/usuario/crear")
    Call<NuevoUsuarioRespuesta> crear(@Body NuevoUsuarioPeticion peticion);

    @POST("api/usuario/recordar")
    //Call<ResponseBody> recordar(@Body RecordarContrasenaPeticion peticion);
    Call<ResponseBody> recordar(@Body RequestBody peticion);
    //Call<RecordarRespuesta> recordar(@Body RecordarContrasenaPeticion peticion);

    @POST("api/usuario/asociarPlaca")
    Call<AsociarPlacaRespuesta> asociarPlaca(@Body AsociarPlacaPeticion asociarPlacaPeticion);
    */

    @GET("Content/publico/documentos/condiciones.html")
    Call<String> obtenerTerminosYCondiciones();
}
