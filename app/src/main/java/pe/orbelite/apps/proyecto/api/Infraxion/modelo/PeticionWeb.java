package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

@Entity(nameInDb = "peticionWeb")
public class PeticionWeb {

    @Id
    @Property(nameInDb = "url")
    private String url;

    @Property(nameInDb = "contenido")
    private String contenido;

    @Property(nameInDb = "creado")
    private Date creado;

    @Generated(hash = 1614224785)
    public PeticionWeb(String url, String contenido, Date creado) {
        this.url = url;
        this.contenido = contenido;
        this.creado = creado;
    }

    @Generated(hash = 283224882)
    public PeticionWeb() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }
}
