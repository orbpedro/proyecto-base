package pe.orbelite.apps.proyecto.mvp.ui.adapters;

public class MiVehiculoItem {

    private String placa;

    private String marca;

    private String modelo;

    private String color;

    private String estado;

    private int totalPapeletas;

    private double totalPagarPapeletas;

    private String soatEstado;
    private String soatInicio;
    private String soatFin;
    private String soatCompania;
    private double soatCosto;
    private String soatClaseVehiculo;
    private String soatUsoVehiculo;
    private String soatTipoCertificado;
    private String titulo;

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getTotalPapeletas() {
        return totalPapeletas;
    }

    public void setTotalPapeletas(int totalPapeletas) {
        this.totalPapeletas = totalPapeletas;
    }

    public double getTotalPagarPapeletas() {
        return totalPagarPapeletas;
    }

    public void setTotalPagarPapeletas(double totalPagarPapeletas) {
        this.totalPagarPapeletas = totalPagarPapeletas;
    }

    public String getSoatEstado() {
        return soatEstado;
    }

    public void setSoatEstado(String soatEstado) {
        this.soatEstado = soatEstado;
    }

    public String getSoatInicio() {
        return soatInicio;
    }

    public void setSoatInicio(String soatInicio) {
        this.soatInicio = soatInicio;
    }

    public String getSoatFin() {
        return soatFin;
    }

    public void setSoatFin(String soatFin) {
        this.soatFin = soatFin;
    }

    public String getSoatCompania() {
        return soatCompania;
    }

    public void setSoatCompania(String soatCompania) {
        this.soatCompania = soatCompania;
    }

    public double getSoatCosto() {
        return soatCosto;
    }

    public void setSoatCosto(double soatCosto) {
        this.soatCosto = soatCosto;
    }

    public String getSoatClaseVehiculo() {
        return soatClaseVehiculo;
    }

    public void setSoatClaseVehiculo(String soatClaseVehiculo) {
        this.soatClaseVehiculo = soatClaseVehiculo;
    }

    public String getSoatUsoVehiculo() {
        return soatUsoVehiculo;
    }

    public void setSoatUsoVehiculo(String soatUsoVehiculo) {
        this.soatUsoVehiculo = soatUsoVehiculo;
    }

    public String getSoatTipoCertificado() {
        return soatTipoCertificado;
    }

    public void setSoatTipoCertificado(String soatTipoCertificado) {
        this.soatTipoCertificado = soatTipoCertificado;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
