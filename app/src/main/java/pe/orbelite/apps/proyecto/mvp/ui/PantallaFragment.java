package pe.orbelite.apps.proyecto.mvp.ui;

import android.view.MenuItem;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseActivity;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;

public class PantallaFragment extends BaseFragment {

    private boolean esRaiz;

    public static final int REQUEST_FINE_LOCATION = 112121212;
    private boolean tieneOpcionAtras = false;

    private Tracker tracker;

    public boolean isTieneOpcionAtras() {
        return tieneOpcionAtras;
    }

    public void setTieneOpcionAtras(boolean tieneOpcionAtras) {
        this.tieneOpcionAtras = tieneOpcionAtras;
    }

    @Override
    public void onResume() {
        super.onResume();
        ocultarTecladoSiEsPosible();

        BaseActivity actividad = (BaseActivity) getActivity();
        actividad.setEsRaiz(isEsRaiz());

        if (esRaiz) {
            actividad.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_panel);
        } else {
            actividad.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }

    public boolean isEsRaiz() {
        return esRaiz;
    }

    public void setEsRaiz(boolean esRaiz) {
        this.esRaiz = esRaiz;
    }



    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                irAtras();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void generarTracker() {
        ProyectoApp application = (ProyectoApp) getActivity().getApplication();
        tracker = application.getDefaultTracker();
    }

    protected  void analyticPagina(String nombrePagina){
        tracker.setScreenName(nombrePagina);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
    */
}
