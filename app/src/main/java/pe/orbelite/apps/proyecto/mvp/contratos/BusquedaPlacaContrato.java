package pe.orbelite.apps.proyecto.mvp.contratos;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Vehiculo;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiVehiculoItem;

public interface BusquedaPlacaContrato {

    interface View extends BaseView {
        void busquedaResultadoOk(Vehiculo vehiculo);
        void busquedaResultadoKo(String mensaje);
    }

    interface Presenter extends BasePresenter<View> {
        void buscarPlaca(String placa);
    }
}
