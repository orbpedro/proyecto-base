package pe.orbelite.apps.proyecto.bd.repositorios.implementaciones;

import android.content.Context;
import android.content.SharedPreferences;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.SharedPreferencesRepositorio;

public class SharedPreferencesRepositorioImpl implements SharedPreferencesRepositorio {

    private SharedPreferences preferences;

    public SharedPreferencesRepositorioImpl(Context context) {
        this.preferences = context.getSharedPreferences(
                ProyectoApp.NOMBRE_PREFERENCIA, Context.MODE_PRIVATE);
    }

    @Override
    public String obtenerTokenDeAcceso() {
        return preferences.getString(ProyectoApp.LLAVE_TOKEN_ACCESO, "");
    }

    @Override
    public void guardarTokenAcceso(String token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ProyectoApp.LLAVE_TOKEN_ACCESO, token);
        editor.commit();
    }
}
