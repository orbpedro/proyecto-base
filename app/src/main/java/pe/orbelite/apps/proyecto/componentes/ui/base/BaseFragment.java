package pe.orbelite.apps.proyecto.componentes.ui.base;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.ErrorGeneral;
import pe.orbelite.apps.proyecto.mvp.ui.MapaFragment;
import pe.orbelite.apps.proyecto.mvp.ui.callbacks.GenerarLlamadaCallback;
import pe.orbelite.apps.proyecto.utilitarios.PermisosUtilitario;

public class BaseFragment extends Fragment implements GenerarLlamadaCallback, OnBackPressed{

    private boolean cargoData = false;
    private String placaLlamada = "";

    public boolean isCargoData() {
        return cargoData;
    }

    public void setCargoData(boolean cargoData) {
        this.cargoData = cargoData;
    }

    protected boolean tieneAccesoGeoVerificados() {

        if (!PermisosUtilitario.verificarListaDePermisosParaUtilizarGeoLocalizacion(getContext())) {
            requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PermisosUtilitario.PEDIR_PERMISO_GEOLOCALIZACION);
            return false;
        }

        return true;
    }

    protected boolean tieneAccesoCamaraVerificados() {

        if (!PermisosUtilitario.verificarListaDePermisosParaUtilizarCamara(getContext())) {
            requestPermissions(new String[]{
                            //Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PermisosUtilitario.PEDIR_PERMISO_CAMARA);
            return false;
        }

        return true;
    }

    protected boolean tieneAccesoLlamadaVerificados() {

        if (!PermisosUtilitario.verificarListaDePermisosParaUtilizarLlamada(getContext())) {
            requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                    },PermisosUtilitario.PEDIR_PERMISO_LLAMADA);
            return false;
        }

        return true;
    }

    protected boolean tieneActividad() {
        return getActivity() != null;
    }

    public void mostrarMensajeSimple(String mensaje) {
        if (tieneActividad()) {
            Toast toast = Toast.makeText(getActivity(),
                    mensaje,
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void mostrarMensajeLargo(String mensaje) {
        if (tieneActividad()) {
            Toast toast = Toast.makeText(getActivity(),
                    mensaje,
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void mostrarMensajeEnDebug(String mensaje) {
        Log.d(getClass().getName(), mensaje);
    }

    protected void cambiarFragment(Fragment fragment) {
        ((BaseActivity) getActivity()).cambiarFragment(fragment);
    }

    protected void removerFragmentDelBack(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(fragment);
        trans.commit();
        manager.popBackStack();

    }

    protected void irAtras() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    protected MaterialDialog.Builder obtenerDialogPorDefecto() {
        return ((BaseActivity) getActivity()).obtenerDialogPorDefecto();
    }

    protected MaterialDialog.Builder obtenerDialogPorDefectoConDosBotones() {
        return ((BaseActivity) getActivity()).obtenerDialogPorDefectoConDosBotones();
    }

    protected void mostrarBarraProgresoIndeterminado(ViewGroup contenedor) {
        if (!tieneActividad()) {
            mostrarMensajeEnDebug("No tiene actividad");
            return;
        }
        if (contenedor == null) {
            mostrarMensajeEnDebug("No tiene contenedor");
            return;
        }

        ocultarBarraProgresoIndeterminado(contenedor);

        View viewBlockScreen = LayoutInflater.from(getActivity()).inflate(R.layout.view_progreso_loading, null, false);

        contenedor.addView(viewBlockScreen);
        mostrarMensajeEnDebug("Agregando loading");

    }


    protected void ocultarBarraProgresoIndeterminado(ViewGroup contenedor) {
        if (!tieneActividad()) {
            return;
        }
        if (contenedor == null) {
            return;
        }

        LinearLayout barraLoading = (LinearLayout) contenedor.findViewById(R.id.barraLoading);
        if (barraLoading == null) {
            return;
        }
        contenedor.removeView(barraLoading);
    }

    public void ocultarTecladoSiEsPosible() {
        if (!tieneActividad()) {
            return;
        }
        EditText edtHidden = getActivity().findViewById(R.id.edtHidden);

        if (edtHidden == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtHidden.getWindowToken(), 0);
    }

    protected void mostrarPantallaBloqueo(String mensaje) {
        if (tieneActividad()) {
            ((BaseActivity) getActivity()).mostrarPantallaBloqueo(mensaje);
        }
    }

    protected void ocultarPantallaBloqueo() {
        if (tieneActividad()) {
            ((BaseActivity) getActivity()).ocultarPantallaBloqueo();
        }
    }

    public void accionGeneralEnErrorAccion() {
        mostrarMensajeSimple("Sucedió un error al procesar la operación");
    }

    public ErrorGeneral obtenerDeJson(String json){

        Gson gson = new Gson();
        ErrorGeneral errorGeneral= gson.fromJson(json, ErrorGeneral.class);
        return errorGeneral;
    }

    protected void llamarTelefonoSoat(String placa){
        this.placaLlamada = placa;
        if(!tieneAccesoLlamadaVerificados()){
            return;
        }
        realizarLlamadaCallback(placa);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermisosUtilitario.PEDIR_PERMISO_LLAMADA: {
                if (PermisosUtilitario.aceptoTodosLosPermisos(grantResults)) {
                    realizarLlamadaCallback(this.placaLlamada);
                } else {
                    noAceptoPermisosCallback(this.placaLlamada);
                }
                return;
            }
        }
    }

    @Override
    public void realizarLlamadaCallback(String placa) {

        List<String> nombres = new ArrayList<>();
        nombres.add("La Positiva"); //4
        nombres.add("Interseguros");  //5
        nombres.add("Mapfre");  //6
        nombres.add("Crecer Seguros");  //11

        obtenerDialogPorDefecto().items(nombres.toArray(new String[nombres.size()])).title(R.string.kontactame).positiveText(R.string.cancelar).itemsCallback(new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {

                switch (position) {
                    case 0:
                        llamarProveedorCallback(placaLlamada, 4);
                        break;
                    case 1:
                        llamarProveedorCallback(placaLlamada, 5);
                        break;
                    case 2:
                        llamarProveedorCallback(placaLlamada, 6);
                        break;
                    case 3:
                        llamarProveedorCallback(placaLlamada, 11);
                        break;
                }
            }
        }).show();
    }

    @Override
    public void noAceptoPermisosCallback(String placa) {
        mostrarMensajeSimple("No puede realizar la llamada, si no habilita los permisos solicitados");
    }

    @Override
    public void llamarProveedorCallback(String placa, int idProveedor) {

    }

    protected void descargarImagenEnImageView(String url, ImageView imageView, final int anchoImagen, final int altoImagen){
        int widthUrl = anchoImagen;
        int heightUrl = altoImagen;
        double width = (double)imageView.getWidth();

        double ratio = width / widthUrl;
        double nuevoAlto = (heightUrl * ratio);

        mostrarMensajeEnDebug("" + ratio + "," + width + " y " + nuevoAlto);

        imageView.getLayoutParams().height = (int) nuevoAlto;
        imageView.requestLayout();

        Glide.with(this).load(url).into(imageView);

    }

    protected void abrirUrl(String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }





    @Override
    public void onBackPressed() {
        Log.d("BaseFragment","Regresando");
    }
}
