package pe.orbelite.apps.proyecto.mvp.contratos;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface DenunciaDetalleImprimirContrato {

    interface View extends BaseView {
        void mostrarDenunciaDatos(Denuncia denuncia);
        void impresoOk();
        void impresoKo();
    }

    interface Presenter extends BasePresenter<DenunciaDetalleImprimirContrato.View> {
        void getDenuncia(String id);
        void putDenunciaImprimir();
    }
}
