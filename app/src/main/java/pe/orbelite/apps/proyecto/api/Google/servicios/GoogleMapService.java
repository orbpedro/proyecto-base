package pe.orbelite.apps.proyecto.api.Google.servicios;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapService {

    @GET("geocode/json")
    Call<Object> obtenerDatosCoordenada(@Query("latlng") String latlng, @Query("key") String key);
}
