package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Vehiculo;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiVehiculoItem;
import pe.orbelite.apps.proyecto.utilitarios.ImagenesAutoUtilitario;

public class VehiculoDetalleFragment extends PantallaFragment {

    private static final String ARG_PLACA = "ARG_PLACA";
    private static final String ARG_MARCA = "ARG_MARCA";
    private static final String ARG_MODELO = "ARG_MODELO";
    private static final String ARG_COLOR = "ARG_COLOR";
    private static final String ARG_ESTADO = "ARG_ESTADO";
    private static final String ARG_TOTAL_PAPELETAS = "ARG_TOTAL_PAPELETAS";
    private static final String ARG_TOTAL_PAGAR_PAPELETAS = "ARG_TOTAL_PAGAR_PAPELETAS";
    private static final String ARG_SOAT_ESTADO = "ARG_SOAT_ESTADO";
    private static final String ARG_SOAT_INICIO = "ARG_SOAT_INICIO";
    private static final String ARG_SOAT_FIN = "ARG_SOAT_FIN";
    private static final String ARG_SOAT_COMPANIA = "ARG_SOAT_COMPANIA";
    private static final String ARG_SOAT_COSTO = "ARG_SOAT_COSTO";
    private static final String ARG_SOAT_CLASE_VEHICULO = "ARG_SOAT_CLASE_VEHICULO";
    private static final String ARG_SOAT_USO_VEHICULO = "ARG_SOAT_USO_VEHICULO";
    private static final String ARG_SOAT_TIPO_CERTIFICADO = "ARG_SOAT_TIPO_CERTIFICADO";


    private String placa;
    private String marca;
    private String modelo;
    private String color;
    private String estado;
    private String soatEstado;
    private String soatInicio;
    private String soatFin;
    private String soatCompania;
    private double soatCosto;
    private String soatClaseVehiculo;
    private String soatUsoVehiculo;
    private String soatTipoCertificado;
    private double totalPagarPapeletas;
    private int totalPapeletas;

    private TextView tvPlaca;
    private TextView tvEstado;
    private TextView tvDescripcion;
    private ImageView ivImagen;
    private ImageView ivSoat;

    private View root;
    FrameLayout contenedorPantalla = null;

    public VehiculoDetalleFragment() {
        // Required empty public constructor
    }

    //public static VehiculoDetalleFragment newInstance(MiVehiculoItem item) {
    public static VehiculoDetalleFragment newInstance(Vehiculo item) {
        VehiculoDetalleFragment fragment = new VehiculoDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PLACA, item.getPlaca());
        args.putString(ARG_MARCA, item.getMarca());
        args.putString(ARG_MODELO, item.getModelo());
        args.putString(ARG_COLOR, item.getColor());
        args.putString(ARG_ESTADO, item.getEstado());
        args.putString(ARG_SOAT_ESTADO, item.getEstado());
        args.putString(ARG_SOAT_INICIO, item.getSoat().getInicio());
        args.putString(ARG_SOAT_FIN, item.getSoat().getFin());
        args.putString(ARG_SOAT_COMPANIA, item.getSoat().getCompania());
        args.putDouble(ARG_SOAT_COSTO, item.getSoat().getCosto());
        args.putString(ARG_SOAT_CLASE_VEHICULO, item.getSoat().getClaseVehiculo());
        args.putString(ARG_SOAT_USO_VEHICULO, item.getSoat().getUsoVehiculo());
        args.putString(ARG_SOAT_TIPO_CERTIFICADO, item.getSoat().getTipoCertificado());
        args.putInt(ARG_TOTAL_PAPELETAS, item.getTotalPapeletas());
        args.putDouble(ARG_TOTAL_PAGAR_PAPELETAS, item.getTotalPagarPapeletas());

        fragment.setArguments(args);
        fragment.setTieneOpcionAtras(true);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            placa = getArguments().getString(ARG_PLACA);
            marca = getArguments().getString(ARG_MARCA);
            modelo = getArguments().getString(ARG_MODELO);
            color = getArguments().getString(ARG_COLOR);
            estado = getArguments().getString(ARG_ESTADO);

            soatEstado = getArguments().getString(ARG_SOAT_ESTADO);
            soatInicio = getArguments().getString(ARG_SOAT_INICIO);
            soatFin = getArguments().getString(ARG_SOAT_FIN);
            soatCompania = getArguments().getString(ARG_SOAT_COMPANIA);
            soatCosto = getArguments().getDouble(ARG_SOAT_COSTO);

            soatClaseVehiculo = getArguments().getString(ARG_SOAT_CLASE_VEHICULO);
            soatUsoVehiculo = getArguments().getString(ARG_SOAT_USO_VEHICULO);
            soatTipoCertificado = getArguments().getString(ARG_SOAT_TIPO_CERTIFICADO);

            totalPagarPapeletas = getArguments().getDouble(ARG_TOTAL_PAGAR_PAPELETAS);
            totalPapeletas = getArguments().getInt(ARG_TOTAL_PAPELETAS);

        }
        //generarTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_vehiculo_detalle, container, false);
        configurarControles();
        ocultarTecladoSiEsPosible();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("");
        setHasOptionsMenu(true);
        //analyticPagina("Detalle Vehiculo");
        cargarDatos();
    }

    private void configurarControles() {
        tvEstado = root.findViewById(R.id.tvEstado);
        tvPlaca = root.findViewById(R.id.tvPlaca);
        tvDescripcion = root.findViewById(R.id.tvDescripcion);
        ivImagen = root.findViewById(R.id.ivImagen);
        ivSoat = root.findViewById(R.id.ivSoat);

        ViewPager viewPager = root.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabs = root.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
    }

    private void cargarDatos() {


        tvPlaca.setText("Vehículo " + placa);
        tvEstado.setText(estado);


        if ("EN CIRCULACION".equalsIgnoreCase(soatEstado)) {
            tvEstado.setTextColor(ContextCompat.getColor(getContext(), R.color.item_soat_vigente));
        } else {
            tvEstado.setTextColor(ContextCompat.getColor(getContext(), R.color.item_soat_vencido));
        }

        String descripcion = "";
        if (marca != null) {
            descripcion += marca + " - ";
        }

        if (modelo != null) {
            descripcion += modelo + ", ";
        }

        if (color != null) {
            descripcion += color;
        }

        tvDescripcion.setText(descripcion);

        ivImagen.setImageResource(ImagenesAutoUtilitario.iconIdDeClaseVehiculo(soatClaseVehiculo));
        ivSoat.setImageResource(ImagenesAutoUtilitario.iconoSoat(soatCompania));


    }

    private void setupViewPager(ViewPager viewPager) {

        MiVehiculoItem item = new MiVehiculoItem();
        item.setSoatCompania(soatCompania);
        item.setSoatEstado(soatEstado);
        item.setSoatClaseVehiculo(soatClaseVehiculo);
        item.setSoatCosto(soatCosto);
        item.setSoatInicio(soatInicio);
        item.setSoatFin(soatFin);
        item.setSoatTipoCertificado(soatTipoCertificado);
        item.setSoatUsoVehiculo(soatUsoVehiculo);
        item.setTotalPagarPapeletas(totalPagarPapeletas);
        item.setTotalPapeletas(totalPapeletas);
        item.setPlaca(placa);

        VehiculoDetalleAdapter adapter = new VehiculoDetalleAdapter(getChildFragmentManager());
        adapter.addFragment(VehiculoSoatFragment.newInstance(item), "Soat");
        adapter.addFragment(VehiculoPapeletasFragment.newInstance(item), "Papeletas");
        viewPager.setAdapter(adapter);
    }


    static class VehiculoDetalleAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public VehiculoDetalleAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
