package pe.orbelite.apps.proyecto.mvp.contratos.base;

public interface BasePresenter<V> {

    void attachView(V view);

    void detachView();
}
