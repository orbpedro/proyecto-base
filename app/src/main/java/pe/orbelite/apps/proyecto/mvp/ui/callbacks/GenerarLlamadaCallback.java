package pe.orbelite.apps.proyecto.mvp.ui.callbacks;

public interface GenerarLlamadaCallback {


    void realizarLlamadaCallback(String placa);

    void noAceptoPermisosCallback(String placa);

    void llamarProveedorCallback(String placa, int idProveedor);

}
