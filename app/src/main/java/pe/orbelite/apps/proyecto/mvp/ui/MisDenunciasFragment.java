package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.mvp.contratos.BusquedaPlacaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MisDenunciasContrato;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.DenunciaItem;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.DenunciaItemAdapter;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.DenunciaListaItem;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiDenunciaItem;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiDenunciaItemAdapter;
import pe.orbelite.apps.proyecto.mvp.ui.controles.RecyclerItemClickListener;

public class MisDenunciasFragment extends PantallaFragment implements MisDenunciasContrato.View {

    @Inject
    MisDenunciasContrato.Presenter presenter;

    private FrameLayout contenedorPantalla;
    private View root;
    RecyclerView listDatos = null;
    LinearLayoutManager manager = null;

    public MisDenunciasFragment() {
        // Required empty public constructor
    }

    public static MisDenunciasFragment newInstance() {
        MisDenunciasFragment fragment = new MisDenunciasFragment();
        fragment.setEsRaiz(true);
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment

        root = inflater.inflate(R.layout.fragment_mis_denuncias, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.titulo_mis_denuncias);

        mostrarBarraProgresoIndeterminado(contenedorPantalla);
        presenter.listarDenuncias();
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        listDatos = root.findViewById(R.id.listDatos);
        listDatos.setHasFixedSize(true);
        manager = new LinearLayoutManager(getActivity());

        listDatos.setLayoutManager(manager);
        listDatos.setAdapter(new MiDenunciaItemAdapter());

    }

    @Override
    public void mostrarDenuncias(List<Denuncia> denuncias) {
        //ocultarBarraProgresoIndeterminado(contenedorPantalla);

        mostrarMensajeEnDebug("Denuncias mostradas " + denuncias.size());

        List<MiDenunciaItem> lista = new ArrayList<>();

        for (Denuncia denuncia : denuncias) {
            MiDenunciaItem item = new MiDenunciaItem();
            item.setId(denuncia.getId());
            item.setEstado(denuncia.getEstado());
            item.setNumero(denuncia.getNumero());
            item.setDireccion(denuncia.getDireccion());
            item.setDescripcion(denuncia.getDescripcion());
            item.setFecha(denuncia.getFecha());
            item.setTitulo("Denuncia #" + denuncia.getNumero());
            item.setUrlFoto(denuncia.getRutaFoto());
            item.setPlaca(denuncia.getPlaca());
            item.setLatitud(denuncia.getLatitud());
            item.setLongitud(denuncia.getLongitud());

            lista.add(item);
        }

        ((MiDenunciaItemAdapter) listDatos.getAdapter()).agregarDatos(lista);
        listDatos.getAdapter().notifyDataSetChanged();
        listDatos.getAdapter().notifyItemRangeChanged(0, listDatos.getAdapter().getItemCount());

        listDatos.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final MiDenunciaItem item = ((MiDenunciaItemAdapter) listDatos.getAdapter()).getDatos().get(position);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switch (item.getEstado()){
                                    case "7":
                                        cambiarFragment(DenunciaDetalleImprimirFragment.newInstance(item.getId()));
                                        break;
                                    default:
                                        cambiarFragment(DenunciaDetalleFragment.newInstance(item.getId()));
                                        break;
                                }
                            }
                        });
                    }
                })
        );

        ocultarBarraProgresoIndeterminado(contenedorPantalla);
    }
}
