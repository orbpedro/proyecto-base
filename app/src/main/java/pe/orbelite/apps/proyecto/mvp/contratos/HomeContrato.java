package pe.orbelite.apps.proyecto.mvp.contratos;

import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface HomeContrato {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<View> {
        void cerrarSesion();
    }

}
