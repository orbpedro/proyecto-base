package pe.orbelite.apps.proyecto.bd.repositorios.implementaciones;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionWeb;
import pe.orbelite.apps.proyecto.bd.repositorios.DaoSession;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;

public class PeticionWebRepositorioImpl implements PeticionWebRepositorio {

    private DaoSession daoSession;

    public PeticionWebRepositorioImpl(DaoSession daoSession) {
        this.daoSession = daoSession;
    }

    @Override
    public void insertarOActualizar(PeticionWeb peticionWeb) {
        daoSession.getPeticionWebDao().insertOrReplace(peticionWeb);
    }

    @Override
    public PeticionWeb buscarPorUrl(String url) {
        return daoSession.getPeticionWebDao().load(url);
    }

    @Override
    public void eliminarPorUrl(String url) {
        daoSession.getPeticionWebDao().deleteByKey(url);
    }

    @Override
    public void eliminarTodo() {
        daoSession.getPeticionWebDao().deleteAll();
    }
}
