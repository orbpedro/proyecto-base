package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Papeleta;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PapeletasPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.PapeletaService;
import pe.orbelite.apps.proyecto.mvp.contratos.VehiculoDetallePapeletasContrato;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class VehiculoDetallePapeletaPresenter implements VehiculoDetallePapeletasContrato.Presenter {

    private WeakReference<VehiculoDetallePapeletasContrato.View> view;
    private Retrofit retrofit;

    @Inject
    public VehiculoDetallePapeletaPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void listarPapeletas(String placa) {

        PapeletasPeticion peticion = new PapeletasPeticion();
        peticion.setPlaca(placa);
        Call<List<Papeleta>> call = retrofit.create(PapeletaService.class).listarPapeletas(peticion);


        view.get().mostrarMensajeEnDebug("Iniciando llamada vehiculos");

        call.enqueue(new Callback<List<Papeleta>>() {
            @Override
            public void onResponse(Call<List<Papeleta>> call, Response<List<Papeleta>> response) {

                if (view.get() == null) {
                    return;
                }


                switch (response.code()) {
                    case 200:

                        view.get().mostrarPapeletas(response.body());
                        break;
                    default:
                        view.get().mostrarMensajeEnDebug(response.raw().message());
                        view.get().accionGeneralEnErrorAccion();
                        break;
                }

            }

            @Override
            public void onFailure(Call<List<Papeleta>> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en llamada" + t.getMessage());
            }
        });

    }

    @Override
    public void attachView(VehiculoDetallePapeletasContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
