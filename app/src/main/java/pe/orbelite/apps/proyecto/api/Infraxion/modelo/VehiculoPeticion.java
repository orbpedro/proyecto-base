package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehiculoPeticion {

    @SerializedName("placa")
    @Expose
    private String placa;

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
}
