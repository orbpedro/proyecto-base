package pe.orbelite.apps.proyecto.mvp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.mvp.contratos.HomeContrato;

public class HomeFragment extends PantallaFragment implements HomeContrato.View {

    @Inject
    HomeContrato.Presenter presenter;

    private View root;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        fragment.setEsRaiz(true);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_home, container, false);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(false);
        confirmarCierre();
    }

    public void confirmarCierre(){
        obtenerDialogPorDefectoConDosBotones()
                .positiveText(R.string.si)
                .negativeText(R.string.no)
                .content(R.string.cerrar_sesion_pregunta)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        cambiarFragment(MapaFragment.newInstance());
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        cerrarSesion();
                    }
                }).show();
    }

    public void cerrarSesion(){
        presenter.cerrarSesion();
        SharedPreferences preferencias = getActivity().getSharedPreferences(
                ProyectoApp.NOMBRE_PREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString(ProyectoApp.LLAVE_TOKEN_ACCESO, "");
        editor.commit();

        startActivity(new Intent(getActivity(), AuthActivity.class));
        getActivity().finish();
    }

}
