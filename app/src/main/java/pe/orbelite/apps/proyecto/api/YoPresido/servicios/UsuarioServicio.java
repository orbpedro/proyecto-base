package pe.orbelite.apps.proyecto.api.YoPresido.servicios;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginRespuesta;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UsuarioServicio {

    //@Headers({"Content-Type: application/json"})
    //@Headers("Accept: application/json")
    //@Headers("Content-Type: application/json")
    //@FormUrlEncoded
    @POST("api/usuario/login/")
    //Call<PeticionResult> loguear(@Field("username") String usuario,@Field("password") String contrasena);
    Call<LoginRespuesta> loguear(@Body LoginPeticion lr);

    /*
    @POST("api/usuario/crear")
    //Call<Usuario> crear();
    Call<PeticionResult> crear(@Body SigninRequest sr);

    @POST("api/usuario/recordar")
    Call<PeticionResult> recordar(@Body RecordarRequest rr);
    */
}
