package pe.orbelite.apps.proyecto.mvp.presenters.base;

import java.lang.ref.WeakReference;

import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;

public abstract class BasePresenterImpl<V> implements BasePresenter<V> {


    private WeakReference<V> view;

    protected V getView(){
        return this.view.get();
    }


    @Override
    public void attachView(V view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
