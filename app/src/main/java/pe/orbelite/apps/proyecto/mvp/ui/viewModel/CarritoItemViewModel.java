package pe.orbelite.apps.proyecto.mvp.ui.viewModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CarritoItemViewModel implements Serializable {


    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    @SerializedName("totalPagar")
    @Expose
    private double totalPagar;

    @SerializedName("cantidad")
    @Expose
    private int cantidad;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(double totalPagar) {
        this.totalPagar = totalPagar;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
