package pe.orbelite.apps.proyecto.di.modulo;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.SharedPreferencesRepositorio;
import pe.orbelite.apps.proyecto.gps.Localizador;
import pe.orbelite.apps.proyecto.mvp.contratos.BusquedaPlacaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleFotosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleImprimirContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.HomeContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.LoginContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaDenunciasContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaMapaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MisDenunciasContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaArchivosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaInformacionContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.VehiculoDetallePapeletasContrato;
import pe.orbelite.apps.proyecto.mvp.presenters.BusquedaPlacaPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.DenunciaDetalleFotosPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.DenunciaDetalleImprimirPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.DenunciaDetallePresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.HomePresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.LoginPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.MapaDenunciasPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.MapaMapaPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.MisDenunciasPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.NuevaDenunciaArchivosPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.NuevaDenunciaInformacionPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.NuevaDenunciaPresenter;
import pe.orbelite.apps.proyecto.mvp.presenters.VehiculoDetallePapeletaPresenter;
import retrofit2.Retrofit;

@Module
public class ProyectoAppModulo {

    private ProyectoApp application;

    public ProyectoAppModulo(ProyectoApp application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return application;
    }


    @Singleton
    @Provides
    public LoginContrato.Presenter provideLoginPresenter(@Named("generalRetrofit") Retrofit retrofit,SharedPreferencesRepositorio sharedPreferencesRepositorio) {
        return new LoginPresenter(retrofit,sharedPreferencesRepositorio);
    }

    @Singleton
    @Provides
    public HomeContrato.Presenter provideHomePresenter(@Named("generalRetrofit") Retrofit retrofit, PeticionWebRepositorio peticionWebRepositorio) {
        return new HomePresenter(retrofit,peticionWebRepositorio);
    }

    @Singleton
    @Provides
    public BusquedaPlacaContrato.Presenter provideBusquedaPlacaPresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new BusquedaPlacaPresenter(retrofit);
    }

    @Singleton
    @Provides
    public VehiculoDetallePapeletasContrato.Presenter provideVehiculoDetallePresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new VehiculoDetallePapeletaPresenter(retrofit);
    }

    @Singleton
    @Provides
    public MisDenunciasContrato.Presenter provideMisDenunciasPresenter(@Named("generalRetrofit") Retrofit retrofit,PeticionWebRepositorio peticionWebRepositorio) {
        return new MisDenunciasPresenter(retrofit,peticionWebRepositorio);
    }

    @Singleton
    @Provides
    public DenunciaDetalleContrato.Presenter provideDenunciaDetallePresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new DenunciaDetallePresenter(retrofit);
    }

    @Singleton
    @Provides
    public DenunciaDetalleFotosContrato.Presenter provideDenunciaDetalleFotosPresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new DenunciaDetalleFotosPresenter(retrofit);
    }


    @Singleton
    @Provides
    public MapaMapaContrato.Presenter provideMapaMapaPresenter(@Named("generalRetrofit") Retrofit retrofit, PeticionWebRepositorio peticionWebRepositorio, Localizador localizador) {
        return new MapaMapaPresenter(retrofit,peticionWebRepositorio,localizador);
    }

    @Singleton
    @Provides
    public MapaDenunciasContrato.Presenter provideMapaDenunciasPresenter(@Named("generalRetrofit") Retrofit retrofit, PeticionWebRepositorio peticionWebRepositorio) {
        return new MapaDenunciasPresenter(retrofit,peticionWebRepositorio);
    }

    @Singleton
    @Provides
    public DenunciaDetalleImprimirContrato.Presenter provideDenunciaDetalleImrpimirPresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new DenunciaDetalleImprimirPresenter(retrofit);
    }

    @Singleton
    @Provides
    public NuevaDenunciaContrato.Presenter provideNuevaDenunciaPresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new NuevaDenunciaPresenter(retrofit);
    }

    @Singleton
    @Provides
    public NuevaDenunciaInformacionContrato.Presenter provideNuevaDenunciaInformacionPresenter(@Named("generalRetrofit") Retrofit retrofit, Localizador localizador, @Named("apiMapRetrofit") Retrofit apiMapRetrofit, PeticionWebRepositorio peticionWebRepositorio) {
        return new NuevaDenunciaInformacionPresenter(retrofit,localizador,apiMapRetrofit,peticionWebRepositorio);
    }

    @Singleton
    @Provides
    public NuevaDenunciaArchivosContrato.Presenter provideNuevaDenunciaArchivosPresenter(@Named("generalRetrofit") Retrofit retrofit) {
        return new NuevaDenunciaArchivosPresenter(retrofit);
    }

}
