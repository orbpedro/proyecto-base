package pe.orbelite.apps.proyecto.mvp.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();

        }
        configurarControles();
    }

    private void configurarControles() {

        SharedPreferences preferencias = this.getSharedPreferences(
                ProyectoApp.NOMBRE_PREFERENCIA, MODE_PRIVATE);

        String token = preferencias.getString(ProyectoApp.LLAVE_TOKEN_ACCESO, "");

        /** SharedPreferences.Editor editor = preferencias.edit();
         editor.putString(InfraxionApp.LLAVE_TOKEN_ACCESO, "FcwLlpCejKv7bMZShwQ9hOI2HNS8HcCVXcqYBz6GdcT4ZnCYhbQizzOQDd0OSx65LBJ85H/sjJS+EPrwGMlML79MaHTXC2cYNVZZLBtrCxnRO9NyfofHawR/3Uo9XKCLwGfiKxaXpfl4MbqmPPombntYd6jNQ4vbx5H7uBUUKAQ=");
         editor.commit();**/
        Intent intent = null;
        if (TextUtils.isEmpty(token)) {
            intent = new Intent(SplashActivity.this, AuthActivity.class);
        } else {
            intent = new Intent(SplashActivity.this, MainActivity.class);
        }

        moverAPantalla(intent);


    }

    private void moverAPantalla(final Intent intent) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}
