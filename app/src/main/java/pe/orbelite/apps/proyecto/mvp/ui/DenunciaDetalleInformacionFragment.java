package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;

public class DenunciaDetalleInformacionFragment extends BaseFragment {

    private static final String ARG_PLACA = "ARG_PLACA";
    private static final String ARG_DESCRIPCION = "ARG_DESCRIPCION";

    private View root;
    private TextView tvPlaca;
    private TextView tvDescripcion;

    private String placa;
    private String descripcion;

    public DenunciaDetalleInformacionFragment() {
        // Required empty public constructor
    }


    public static DenunciaDetalleInformacionFragment newInstance(String placa,String descripcion) {
        DenunciaDetalleInformacionFragment fragment = new DenunciaDetalleInformacionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PLACA, placa);
        args.putString(ARG_DESCRIPCION, descripcion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            placa = getArguments().getString(ARG_PLACA);
            descripcion = getArguments().getString(ARG_DESCRIPCION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_denuncia_detalle_informacion, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ocultarTecladoSiEsPosible();
        cargarDatos();
    }

    private void configurarControles() {
        tvPlaca = root.findViewById(R.id.tvPlaca);
        tvDescripcion = root.findViewById(R.id.tvDescripcion);
    }

    private void cargarDatos(){
        tvPlaca.setText(placa);
        tvDescripcion.setText(descripcion);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            mostrarMensajeEnDebug("Tab Visible");
            ocultarTecladoSiEsPosible();
        }else{
            //mostrarMensajeEnDebug("Tab Oculto");
        }
    }

}
