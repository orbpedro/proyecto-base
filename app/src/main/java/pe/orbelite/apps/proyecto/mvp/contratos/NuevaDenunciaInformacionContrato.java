package pe.orbelite.apps.proyecto.mvp.contratos;

import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface NuevaDenunciaInformacionContrato {

    interface View extends BaseView {
        void mostrarRutaMapa(double latitud, double longitud);
        void mostrarLugar(String lugar);
        void errorAlConseguirLugar();
        void errorAlConseguirDatosGeo();
        void mostrarBarraEjecucion();
        void ocultarBarraEjecucion();
        boolean tieneAccesoGeo();
    }

    interface Presenter extends BasePresenter<NuevaDenunciaInformacionContrato.View> {
        void iniciarGeoLocalizacion();
    }

}
