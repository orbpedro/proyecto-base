package pe.orbelite.apps.proyecto.bd.repositorios.abstracciones;

public interface SharedPreferencesRepositorio {

    String obtenerTokenDeAcceso();
    void guardarTokenAcceso(String token);
}
