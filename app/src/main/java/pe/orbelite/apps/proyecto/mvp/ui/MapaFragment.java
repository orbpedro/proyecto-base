package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pe.orbelite.apps.proyecto.R;

public class MapaFragment extends PantallaFragment {

    private View root;

    public MapaFragment() {
        // Required empty public constructor
    }

    public static MapaFragment newInstance() {
        MapaFragment fragment = new MapaFragment();
        fragment.setEsRaiz(true);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_mapa, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    private void configurarControles() {
        ViewPager viewPager = root.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabs = root.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {

        MapaAdapter adapter = new MapaAdapter(getChildFragmentManager());
        adapter.addFragment(MapaMapaFragment.newInstance(), "Mapa");
        adapter.addFragment(MapaDenunciasFragment.newInstance(), "Lista");
        viewPager.setAdapter(adapter);
    }

    static class MapaAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public MapaAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
