package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Vehiculo;
import pe.orbelite.apps.proyecto.mvp.contratos.BusquedaPlacaContrato;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiVehiculoItem;

public class BusquedaPlacaFragment extends PantallaFragment implements BusquedaPlacaContrato.View {

    @Inject
    BusquedaPlacaContrato.Presenter presenter;

    private View root;
    FrameLayout contenedorPantalla = null;
    EditText edtPlaca;
    Button btnBuscar;

    public BusquedaPlacaFragment() {
        // Required empty public constructor
    }

    public static BusquedaPlacaFragment newInstance() {
        BusquedaPlacaFragment fragment = new BusquedaPlacaFragment();
        fragment.setEsRaiz(true);
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
        //generarTracker();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_busqueda_placa, container, false);
        configurarControles();

        mostrarMensajeEnDebug("Fragment búsqueda de placa");

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.seccion_busqueda);
        edtPlaca.requestFocus();
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        edtPlaca=root.findViewById(R.id.edtPlaca);
        btnBuscar=root.findViewById(R.id.btnBuscar);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBuscar.setEnabled(false);
                String placa = edtPlaca.getText().toString();
                placa = placa.replace(" ", "");
                placa = placa.replace("-", "");
                placa = placa.toLowerCase();
                placa = placa.trim();

                presenter.buscarPlaca(placa);
                mostrarMensajeEnDebug(placa);
            }
        });
    }

    @Override
    public void busquedaResultadoOk(Vehiculo vehiculo) {
        VehiculoDetalleFragment fragment = VehiculoDetalleFragment.newInstance(vehiculo);

        cambiarFragment(fragment);
        btnBuscar.setEnabled(true);
    }

    @Override
    public void busquedaResultadoKo(String mensaje) {
        mostrarMensajeSimple(mensaje);
        btnBuscar.setEnabled(true);
    }
}
