package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;

public class NuevaDenunciaInfraxionesFragment extends BaseFragment {

    private LinearLayout contenedorInfraxiones;
    private View root;


    public NuevaDenunciaInfraxionesFragment() {
        // Required empty public constructor
    }

    public static NuevaDenunciaInfraxionesFragment newInstance() {
        NuevaDenunciaInfraxionesFragment fragment = new NuevaDenunciaInfraxionesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_nueva_denuncia_infraxiones, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarDatos();
    }

    private void configurarControles(){
        contenedorInfraxiones = root.findViewById(R.id.contenedorInfraxiones);
    }

    private void cargarDatos(){
        contenedorInfraxiones.removeAllViews();
        mostrarInfraxionEjemplo("Infraxion M01", "17-05-2018", "Conducir con presencia de alcohol en la sangre en proporción mayor a lo previsto en el Código Penal, o bajo los efectos de estupefacientes, narcóticos y/o alucinógenos comprobado con el exámen respectivo o por negarse al mismo y que haya participado en un accidente de tránsito.");
        mostrarInfraxionEjemplo("Infraxion M06", "17-05-2018", "Estacionar en las curvas, puentes, túneles, zonas estrechas de la vía, pasos a nivel, pasos a desnivel en cambios de rasante, pendientes y cruces de ferrocarril.");

    }

    private void mostrarInfraxionEjemplo(String titulo, String fecha, String descripcion) {
        View infraxionView = LayoutInflater.from(getActivity()).inflate(R.layout.view_denuncia_infraxion, null, false);
        TextView tvTitulo = infraxionView.findViewById(R.id.tvTitulo);
        TextView tvFecha = infraxionView.findViewById(R.id.tvFecha);
        TextView tvDescripcion = infraxionView.findViewById(R.id.tvDescripcion);


        tvTitulo.setText(titulo);
        tvFecha.setText(fecha);
        tvDescripcion.setText(descripcion);

        contenedorInfraxiones.addView(infraxionView);
    }


}
