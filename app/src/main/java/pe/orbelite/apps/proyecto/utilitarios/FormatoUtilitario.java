package pe.orbelite.apps.proyecto.utilitarios;

import android.text.TextUtils;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class FormatoUtilitario {

    public static Date fechaDeTexto(String cadena, String formato) {
        Date fecha = null;

        SimpleDateFormat format = new SimpleDateFormat(formato);

        try {
            fecha = format.parse(cadena);
        } catch (Exception ex) {
            fecha = null;
        }

        return fecha;
    }

    public static <E> E objetoDeJson(String json, Class<E> clase) {

        if (TextUtils.isEmpty(json)) {
            return null;
        }

        Gson gson = new Gson();
        E entidad = gson.fromJson(json, clase);
        return entidad;
    }

    public static <E> String jsonDeObjeto(E objeto) {

        if (objeto == null) {
            return null;
        }

        Gson gson = new Gson();
        String json = gson.toJson(objeto);
        return json;
    }

    public static <E> List<E> listaDeJson(String json, Class<E[]> clase) {

        if (TextUtils.isEmpty(json)) {
            return null;
        }

        Gson gson = new Gson();
        E[] lista = gson.fromJson(json, clase);
        return Arrays.asList(lista);
    }

    public static <E> String datoDefault(E dato){
        if(dato == null){
            return  "";
        }
        return  dato.toString();
    }
}
