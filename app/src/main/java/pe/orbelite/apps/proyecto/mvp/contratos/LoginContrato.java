package pe.orbelite.apps.proyecto.mvp.contratos;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.LoginRespuesta;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface LoginContrato {

    interface View extends BaseView {
        void loguear();
        void logueoOk(LoginRespuesta loginRespuesta);
        void logueoKo(String respuesta);
    }

    interface Presenter extends BasePresenter<View>{
        void validarLogueo(LoginPeticion loginPeticion);
    }

}
