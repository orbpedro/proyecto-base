package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseActivity;

public class AuthActivity extends BaseActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        toolbar = findViewById(R.id.toolbarPrincipal);
        setSupportActionBar(toolbar);

        LoginFragment fragment = LoginFragment.newInstance();
        cambiarFragment(fragment);
    }

}
