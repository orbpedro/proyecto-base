package pe.orbelite.apps.proyecto.mvp.ui.dialogos;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import pe.orbelite.apps.proyecto.R;


public class DetalleMapaDenunciaDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ID= "ARG_ID";
    private static final String ARG_URL_IMAGEN = "ARG_URL_IMAGEN";
    private static final String ARG_TITULO = "ARG_TITULO";
    private static final String ARG_FECHA = "ARG_FECHA";
    private static final String ARG_DESCRIPCION = "ARG_DESCRIPCION";
    private static final String ARG_ESTADO = "ARG_ESTADO";
    private static final String ARG_DIRECCION = "ARG_DIRECCION";

    private OnDialogClickListener listener;



    // TODO: Rename and change types of parameters
    private String id;
    private String urlImagen;
    private String titulo;
    private String fecha;
    private String descripcion;
    private String estado;
    private String direccion;

    private View root = null;
    private TextView tvEstado;
    private TextView tvFecha;
    private TextView tvDireccion;
    private TextView tvDescripcion;
    private ImageView ivFoto;

    public DetalleMapaDenunciaDialogFragment() {
        // Required empty public constructor
    }

    public OnDialogClickListener getListener() {
        return listener;
    }

    public void setListener(OnDialogClickListener listener) {
        this.listener = listener;
    }


    public static DetalleMapaDenunciaDialogFragment newInstance(String id,String titulo, String urlImagen, String fecha, String descripcion, String estado, String direccion, OnDialogClickListener listener) {
        DetalleMapaDenunciaDialogFragment fragment = new DetalleMapaDenunciaDialogFragment();
        fragment.setListener(listener);
        Bundle args = new Bundle();
        args.putString(ARG_ID, id);
        args.putString(ARG_URL_IMAGEN, urlImagen);
        args.putString(ARG_TITULO, titulo);
        args.putString(ARG_FECHA, fecha);
        args.putString(ARG_DESCRIPCION, descripcion);
        args.putString(ARG_ESTADO, estado);
        args.putString(ARG_DIRECCION, direccion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_ID);
            urlImagen = getArguments().getString(ARG_URL_IMAGEN);
            titulo = getArguments().getString(ARG_TITULO);
            fecha = getArguments().getString(ARG_FECHA);
            descripcion = getArguments().getString(ARG_DESCRIPCION);
            estado = getArguments().getString(ARG_ESTADO);
            direccion = getArguments().getString(ARG_DIRECCION);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        root = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_dialog_detalle_mapa_denuncia, null, false);
        Log.i("ESTADO EN DIALOG",estado);
        configurarControles();
        cargarDatos();

        MaterialDialog dialogo=null;
        switch (estado){
            case "Reportarda":
                dialogo = new MaterialDialog.Builder(getActivity())
                        .theme(Theme.LIGHT)
                        .title(titulo)
                        .positiveColorRes(R.color.rojo)
                        .negativeColorRes(R.color.negro)
                        .customView(root, false)
                        .negativeText("Cerrar")
                        .cancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        })
                        .positiveText("Gestionar")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (listener == null) {
                                    return;
                                }
                                dismiss();
                                listener.onDialogGestionarClick(id);
                            }
                        })
                        .build();
                break;
            default:
                dialogo = new MaterialDialog.Builder(getActivity())
                        .theme(Theme.LIGHT)
                        .title(titulo)
                        .positiveColorRes(R.color.rojo)
                        .negativeColorRes(R.color.negro)
                        .customView(root, false)
                        .negativeText("Cerrar")
                        .cancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        })
                        .positiveText("Ver")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (listener == null) {
                                    return;
                                }
                                dismiss();
                                listener.onDialogVerClick(id);
                            }
                        })
                        .build();
                break;
        }

        return dialogo;
    }

    private void configurarControles(){
        tvDireccion = root.findViewById(R.id.tvDireccion);
        tvEstado = root.findViewById(R.id.tvEstado);
        tvFecha = root.findViewById(R.id.tvFecha);
        tvDescripcion = root.findViewById(R.id.tvDescripcion);
        ivFoto = root.findViewById(R.id.ivFoto);

    }

    private void cargarDatos(){
        tvFecha.setText(fecha);
        tvEstado.setText(estado);
        tvDireccion.setText(direccion);
        tvDescripcion.setText(descripcion);
        Picasso.get().load(urlImagen).resize(80,80).centerCrop().into(ivFoto);
    }

    public interface OnDialogClickListener {
        void onDialogGestionarClick(String id);
        void onDialogVerClick(String id);
    }

}
