package pe.orbelite.apps.proyecto.mvp.presenters;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Peticion;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.DenunciaService;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.mvp.contratos.MisDenunciasContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MisDenunciasContrato.Presenter;
import pe.orbelite.apps.proyecto.utilitarios.FormatoUtilitario;
import pe.orbelite.apps.proyecto.utilitarios.PeticionWebUtilitario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MisDenunciasPresenter implements Presenter {

    private WeakReference<MisDenunciasContrato.View> view;
    private Retrofit retrofit;
    private PeticionWebRepositorio peticionWebRepositorio;

    @Inject
    public MisDenunciasPresenter(Retrofit retrofit,PeticionWebRepositorio peticionWebRepositorio) {
        this.retrofit = retrofit;
        this.peticionWebRepositorio = peticionWebRepositorio;
    }

    @Override
    public void listarDenuncias() {
        //view.get().mostrarMensajeEnDebug("Listar denuncias");

        if (leidoDeCache()) {
            return;
        }

        Peticion peticion = new Peticion();
        Call<List<Denuncia>> call =   retrofit.create(DenunciaService.class).listarMisDenuncias(peticion);

        //view.get().mostrarMensajeEnDebug("Iniciando peticion");

        call.enqueue(new Callback<List<Denuncia>>() {
            @Override
            public void onResponse(Call<List<Denuncia>> call, Response<List<Denuncia>> response) {

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        listarDenunciasOk(response.body());
                        break;

                }

            }

            @Override
            public void onFailure(Call<List<Denuncia>> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en llamada" + t.getMessage());
            }
        });

    }

    private boolean leidoDeCache() {

        if (view.get() == null) {
            return false;
        }

        String json = PeticionWebUtilitario.obtenerJsonDePeticionWeb(peticionWebRepositorio, "denunciaVehicular/obtener_mis_denuncias");
        List<Denuncia> denuncias = FormatoUtilitario.listaDeJson(json, Denuncia[].class);

        if (denuncias != null) {
            view.get().mostrarDenuncias(denuncias);
            return true;
        }

        return false;
    }

    private void listarDenunciasOk(List<Denuncia> denuncias) {
        view.get().mostrarDenuncias(denuncias);
        PeticionWebUtilitario.grabarContenidoPeticion(peticionWebRepositorio, "denuncia/obtener_denuncias", FormatoUtilitario.jsonDeObjeto(denuncias));
    }

    @Override
    public void attachView(MisDenunciasContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
