package pe.orbelite.apps.proyecto.api.Infraxion.servicios;

import okhttp3.RequestBody;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.SubirImagenRespuesta;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ArchivoService {

    @Multipart
    @POST("api/archivo/subir_archivo")
    retrofit2.Call<SubirImagenRespuesta> subirCorte(@Part("archivo\"; filename=\"foto.png") RequestBody foto);
    //retrofit2.Call<ResponseBody> subirCorte(@Part("archivo\"; filename=\"foto.png") RequestBody foto);
    //retrofit2.Call<JsonObject> subirCorte(@Part("archivo\"; filename=\"foto.png") RequestBody foto);
    //retrofit2.Call<Object> subirCorte(@Part("archivo\"; filename=\"foto.png") RequestBody foto);
}
