package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PeticionResult {
    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("codigoError")
    @Expose
    private int errorCod;

    @SerializedName("errores")
    @Expose
    private ArrayList<String> errorLis;

    @SerializedName("mensaje")
    @Expose
    private String mensaje;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getErrorCod() {
        return errorCod;
    }

    public void setErrorCod(int errorCod) {
        this.errorCod = errorCod;
    }

    public ArrayList<String> getErrorLis() {
        return errorLis;
    }

    public void setErrorLis(ArrayList<String> errorLis) {
        this.errorLis = errorLis;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
