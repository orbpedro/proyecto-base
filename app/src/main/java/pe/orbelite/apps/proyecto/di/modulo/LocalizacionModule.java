package pe.orbelite.apps.proyecto.di.modulo;

import android.content.Context;
import android.support.annotation.NonNull;

import org.greenrobot.greendao.annotation.NotNull;

import dagger.Module;
import dagger.Provides;
import pe.orbelite.apps.proyecto.gps.Localizador;

@Module
public class LocalizacionModule {

    @Provides
    @NonNull
    public Localizador provideLocalizador(@NotNull Context context){
        return new Localizador(context);
    }
}
