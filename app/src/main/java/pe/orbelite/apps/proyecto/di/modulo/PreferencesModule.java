package pe.orbelite.apps.proyecto.di.modulo;

import android.content.Context;
import android.support.annotation.NonNull;

import org.greenrobot.greendao.annotation.NotNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.SharedPreferencesRepositorio;
import pe.orbelite.apps.proyecto.bd.repositorios.implementaciones.SharedPreferencesRepositorioImpl;

@Module
public class PreferencesModule {

    @Provides
    @NonNull
    @Singleton
    public SharedPreferencesRepositorio provideSharedPreferencesRepositorio(@NotNull Context context) {
        return new SharedPreferencesRepositorioImpl(context);
    }
}
