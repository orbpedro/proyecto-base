package pe.orbelite.apps.proyecto.mvp.ui.dialogos;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Infraccion;

public class ListaDenunciaInfraccionesDialogFragment extends DialogFragment {

    private static final String ARG_INFRACCIONES_MAPA = "ARG_INFRACCIONES_MAPA";

    private OnDialogClickListener listener;

    private View root = null;
    private LinearLayout contenedorInfraxiones=null;
    private TextView tvTitulo = null;
    private TextView tvFecha = null;
    private TextView tvDescripcion = null;

    private List<Infraccion> infracciones;
    private HashMap<String, Integer> infraccionMap;

    public ListaDenunciaInfraccionesDialogFragment() {
        // Required empty public constructor
    }

    public static ListaDenunciaInfraccionesDialogFragment newInstance(Serializable infraccionMap,
            OnDialogClickListener listener) {
        ListaDenunciaInfraccionesDialogFragment fragment = new ListaDenunciaInfraccionesDialogFragment();
        fragment.setListener(listener);
        Bundle args = new Bundle();
        args.putSerializable(ARG_INFRACCIONES_MAPA, infraccionMap);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            infraccionMap = (HashMap<String, Integer>) getArguments().getSerializable(ARG_INFRACCIONES_MAPA);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        root = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_dialog_lista_denuncia_infracciones, null, false);

        configurarControles();
        cargarDatos();

        MaterialDialog dialogo = new MaterialDialog.Builder(getActivity()).theme(Theme.LIGHT)
                .title("Infracciones").positiveColorRes(R.color.rojo).negativeColorRes(R.color.negro)
                .customView(root, false)
                .negativeText("Cerrar")
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        return;
                    }
                }).build();

        return dialogo;
    }

    private void configurarControles(){
        contenedorInfraxiones = root.findViewById(R.id.contenedorInfraxiones);
        tvTitulo = root.findViewById(R.id.tvTitulo);
        tvFecha = root.findViewById(R.id.tvFecha);
        tvDescripcion = root.findViewById(R.id.tvDescripcion);


    }

    private void cargarDatos(){
        if(infracciones==null){
            infracciones = new ArrayList<>();

            Infraccion inf01=new Infraccion();
            inf01.setCategoria("M01");
            //inf01.setFecha("17/05/2018");
            inf01.setObservacion("Conducir con presencia de alcohol en la sangre en proporción mayor a lo previsto en el Código Penal, o bajo los efectos de estupefacientes, narcóticos y/o alucinógenos comprobado con el exámen respectivo o por negarse al mismo y que haya participado en un accidente de tránsito.");

            Infraccion inf02=new Infraccion();
            inf02.setCategoria("M06");
            //inf02.setFecha("28/12/2018");
            inf02.setObservacion("Estacionar en las curvas, puentes, túneles, zonas estrechas de la vía, pasos a nivel, pasos a desnivel en cambios de rasante, pendientes y cruces de ferrocarril.");

            Infraccion inf03=new Infraccion();
            inf03.setCategoria("M09");
            //inf03.setFecha("27/12/2018");
            inf03.setObservacion("Estacionar en las curvas, puentes, túneles, zonas estrechas de la vía, pasos a nivel, pasos a desnivel en cambios de rasante, pendientes y cruces de ferrocarril.");

            infracciones.add(inf01);
            infracciones.add(inf02);
            infracciones.add(inf03);

            for (Infraccion infraccion : infracciones) {
                poblarLista(infraccion);
            }
        }
    }

    private void poblarLista(final Infraccion infraccion){
        View miInfraccion = LayoutInflater.from(getActivity()).inflate(R.layout.view_item_infraccion, null, false);
        TextView titulo = miInfraccion.findViewById(R.id.tvTitulo);
        TextView descripcion = miInfraccion.findViewById(R.id.tvDescripcion);

        final Button agregar = miInfraccion.findViewById(R.id.btnInfraccionAgregar);
        final Button quitar = miInfraccion.findViewById(R.id.btnInfraccionQuitar);

        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDialogInfraccionAgregarClick(infraccion.getCategoria(),
                        infraccion.getObservacion());
                agregar.setVisibility(View.GONE);
                agregar.setEnabled(false);
                quitar.setVisibility(View.VISIBLE);
                quitar.setEnabled(true);
                infraccionMap.put(infraccion.getCategoria(),infraccionMap.size()+1);
            }
        });

        quitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDialogInfraccionQuitarClick(infraccion.getCategoria());
                quitar.setVisibility(View.GONE);
                quitar.setEnabled(false);
                agregar.setVisibility(View.VISIBLE);
                agregar.setEnabled(true);
                infraccionMap.remove(infraccion.getCategoria());
            }
        });

        titulo.setText(infraccion.getCategoria());
        descripcion.setText(infraccion.getObservacion());

        if(infraccionMap.containsKey(infraccion.getCategoria())) {
            agregar.setVisibility(View.GONE);
            agregar.setEnabled(false);
            quitar.setVisibility(View.VISIBLE);
            quitar.setEnabled(true);
        }

        contenedorInfraxiones.addView(miInfraccion);
    }

    public OnDialogClickListener getListener() {
        return listener;
    }

    public void setListener(OnDialogClickListener listener) {
        this.listener = listener;
    }

    public interface OnDialogClickListener {
        void onDialogInfraccionAgregarClick(String id,String descripcion);

        void onDialogInfraccionQuitarClick(String id);
    }
}
