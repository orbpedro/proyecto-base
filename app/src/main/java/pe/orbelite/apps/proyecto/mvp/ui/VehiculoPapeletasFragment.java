package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Papeleta;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.contratos.VehiculoDetallePapeletasContrato;
import pe.orbelite.apps.proyecto.mvp.ui.adapters.MiVehiculoItem;
import pe.orbelite.apps.proyecto.mvp.ui.viewModel.CarritoItemViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VehiculoPapeletasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehiculoPapeletasFragment extends BaseFragment implements VehiculoDetallePapeletasContrato.View {

    private static final String ARG_TOTAL_PAPELETAS = "ARG_TOTAL_PAPELETAS";
    private static final String ARG_TOTAL_PAGAR_PAPELETAS = "ARG_TOTAL_PAGAR_PAPELETAS";
    private static final String ARG_PLACA = "ARG_PLACA";

    private double totalPagarPapeletas;
    private int totalPapeletas;
    private String placa;
    private List<Papeleta> papeletas;

    private View root;
    private TextView tvTotalPapeletas;
    private TextView tvTotalAPagar;
    private FrameLayout contenedorPantalla;
    private LinearLayout contenedorPapeletas;
    private Button btnPagar;

    @Inject
    VehiculoDetallePapeletasContrato.Presenter presenter;

    public VehiculoPapeletasFragment() {
        // Required empty public constructor
    }

    public static VehiculoPapeletasFragment newInstance(MiVehiculoItem item) {
        VehiculoPapeletasFragment fragment = new VehiculoPapeletasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TOTAL_PAPELETAS, item.getTotalPapeletas());
        args.putDouble(ARG_TOTAL_PAGAR_PAPELETAS, item.getTotalPagarPapeletas());
        args.putString(ARG_PLACA, item.getPlaca());

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            totalPagarPapeletas = getArguments().getDouble(ARG_TOTAL_PAGAR_PAPELETAS);
            totalPapeletas = getArguments().getInt(ARG_TOTAL_PAPELETAS);
            placa = getArguments().getString(ARG_PLACA);
        }


        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_vehiculo_papeletas, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarDatos();
    }

    private void configurarControles() {
        tvTotalPapeletas = root.findViewById(R.id.tvTotalPapeletas);
        tvTotalAPagar = root.findViewById(R.id.tvTotalAPagar);
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        contenedorPapeletas = root.findViewById(R.id.contenedorPapeletas);
        btnPagar = root.findViewById(R.id.btnPagar);

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagarPapeletas();
            }
        });
    }

    private void cargarDatos() {
        //tvTotalPapeletas.setText("" + totalPapeletas);
        //tvTotalAPagar.setText("" + totalPagarPapeletas);
        actualizarDatosPapeletas();
        mostrarBarraProgresoIndeterminado(contenedorPantalla);
        presenter.listarPapeletas(placa);
    }

    private void actualizarDatosPapeletas(){
        tvTotalPapeletas.setText("" + totalPapeletas);
        tvTotalAPagar.setText("s/. "+String.format("%.2f", totalPagarPapeletas));

        /*
        if(totalPapeletas>0
                && totalPagarPapeletas>0) mostrarPagar();
        */
    }

    private void mostrarPagar(){
        btnPagar.setEnabled(true);
        btnPagar.setVisibility(View.VISIBLE);
    }

    @Override
    public void mostrarPapeletas(List<Papeleta> papeletas) {
        contenedorPapeletas.removeAllViews();
        ocultarBarraProgresoIndeterminado(contenedorPantalla);

        this.papeletas = papeletas;

        mostrarMensajeEnDebug("papeletas encontradas: " + papeletas.size());
        totalPapeletas=papeletas.size();

        for (Papeleta papeleta : papeletas) {
            View papeletaView = LayoutInflater.from(getActivity()).inflate(R.layout.view_item_papeleta, null, false);
            TextView tvTitulo = papeletaView.findViewById(R.id.tvTitulo);
            TextView tvFecha = papeletaView.findViewById(R.id.tvFecha);
            TextView tvEstado = papeletaView.findViewById(R.id.tvEstado);
            //TextView tvTotal = papeletaView.findViewById(R.id.tvTotal);

            tvTitulo.setText(papeleta.getFalta());
            tvFecha.setText(papeleta.getFechaEmision() != null ? papeleta.getFechaEmision() : "");
            tvEstado.setText(papeleta.getEstado() != null ? papeleta.getEstado() : "");
            //tvTotal.setText(papeleta.getTotal() != null ? papeleta.getTotal() : "");

            totalPagarPapeletas=totalPagarPapeletas+Double.parseDouble(papeleta.getTotal());

            contenedorPapeletas.addView(papeletaView);
        }

        actualizarDatosPapeletas();
    }

    @Override
    public void accionGeneralEnErrorAccion() {
        super.accionGeneralEnErrorAccion();
        ocultarBarraProgresoIndeterminado(contenedorPantalla);
    }

    private void pagarPapeletas() {

        if (this.papeletas == null || this.papeletas.size() == 0) {
            return;
        }

        List<CarritoItemViewModel> listaCarrito = new ArrayList<>();

        for (Papeleta papeleta : this.papeletas) {
            CarritoItemViewModel item = new CarritoItemViewModel();
            String descripcion = "";
            double precio = 0;
            if (papeleta.getFalta() != null) {
                descripcion += "Papeleta - " + papeleta.getFalta() + " para placa " + papeleta.getPlaca();
            }

            try {
                precio = Double.parseDouble(papeleta.getTotal());
            } catch (Exception ex) {
                precio = 0;
            }


            item.setCantidad(1);
            item.setDescripcion(descripcion);
            item.setTotalPagar(precio);
            listaCarrito.add(item);
        }

        //cambiarFragment(CarritoSimpleFragment.newInstance((Serializable) listaCarrito));

    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}
