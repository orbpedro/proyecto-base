package pe.orbelite.apps.proyecto.mvp.ui;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Foto;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Infraccion;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleFotosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaArchivosContrato;
import pe.orbelite.apps.proyecto.utilitarios.PermisosUtilitario;

public class DenunciaDetalleFotosFragment extends PantallaFragment implements DenunciaDetalleFotosContrato.View {

    private static final String ARG_FOTOS = "ARG_FOTOS";

    @Inject
    DenunciaDetalleFotosContrato.Presenter presenter;

    FrameLayout contenedorPantalla = null;
    private LinearLayout contenedorFotos;
    private View root;
    private Button btnAgregarFoto;
    private ImageView ivFoto;

    private Uri fotoURI;
    private Uri fotoCortadaURI;
    private File fotoArchivo;
    private File fotoCortadaArchivo;
    private Bitmap fotoCortadaImagen;

    private List<Foto> fotos = new ArrayList<Foto>();

    private OnDenunciaFotosListener listener;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;

    public DenunciaDetalleFotosFragment() {
        // Required empty public constructor
    }

    public static DenunciaDetalleFotosFragment newInstance(Serializable fotos,
                                                           OnDenunciaFotosListener listener) {
        //Log.i("ESTADO","Creando instancia");
        DenunciaDetalleFotosFragment fragment = new DenunciaDetalleFotosFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FOTOS, fotos);
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fotos = (List<Foto>) getArguments().getSerializable(ARG_FOTOS);
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_denuncia_detalle_fotos, container, false);
        configurarControles();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        //mostrarMensajeEnDebug("On Pause...");
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        contenedorFotos=root.findViewById(R.id.llContenedorDenunciaFotos);
        btnAgregarFoto = root.findViewById(R.id.btnAgregar);
        btnAgregarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mostrarPantallaBloqueo("Bloqueando :v");
                abrirCamara();
            }
        });
    }

    @Override
    public void verficarCamara() {
        btnAgregarFoto.setEnabled(false);

        Context context = getActivity();
        PackageManager packageManager = context.getPackageManager();
        if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
            mostrarMensajeSimple("Este equipo no tiene disponible una cámara.");
            return;
        }

        if(!tieneAccesoCamaraVerificados()){
            return ;
        }
    }

    @Override
    public void abrirCamara() {
        btnAgregarFoto.setEnabled(false);
        btnAgregarFoto.setVisibility(View.GONE);
        mostrarMensajeEnDebug("Funcion Abrir camara");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            //Create a file to store the image
            try {
                fotoArchivo = crearArchivoImagenVacio();
            } catch (IOException ex) {
                // Error occurred while creating the File
                mostrarMensajeEnDebug("Error al crear achivo de imagen vacío.");
            }
            if (fotoArchivo != null) {
                fotoURI = FileProvider.getUriForFile(getContext(),
                        "pe.orbelite.apps.proyecto.fileprovider",
                        fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        fotoURI);

                startActivityForResult(intent,CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        }
    }

    @Override
    public void mostrarRecorte(File file) {
        //Bitmap miImagen = BitmapFactory.decodeFile(file.getAbsolutePath());
        //ivFoto.setImageBitmap(miImagen);
        ocultarBarraProgresoIndeterminado(contenedorPantalla);

        View denunciaFotoView = LayoutInflater.from(getActivity()).inflate(R.layout.view_item_denuncia_foto, null, false);

        ImageView ivDenunciaFoto = denunciaFotoView.findViewById(R.id.ivItemDenunciaFoto);
        ivDenunciaFoto.setImageBitmap(fotoCortadaImagen);

        contenedorFotos.addView(denunciaFotoView);
        denunciaFotoView.requestFocus();
        btnAgregarFoto.setEnabled(true);
    }

    @Override
    public void subirCorteOk(String archivoId) {
        mostrarRecorte(null);
        //mostrarMensajeSimple("Se subió el corte exitosamente: "+archivoId);
        ocultarPantallaBloqueo();
        ocultarBarraProgresoIndeterminado(contenedorFotos);
        listener.getFoto(archivoId);
        limpiarVariables();
    }

    @Override
    public void subirCorteKo() {
        btnAgregarFoto.setEnabled(true);
        ocultarBarraProgresoIndeterminado(contenedorFotos);
        mostrarMensajeSimple("Hubo un problema al subir el corte, verifique su conexión a internet intente de nuevo por favor.");
        ocultarPantallaBloqueo();
    }

    String ArchivoImagenVacioRuta;
    private File crearArchivoImagenVacio() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        ArchivoImagenVacioRuta = image.getAbsolutePath();
        mostrarMensajeEnDebug("Ruta imagen vacía "+ArchivoImagenVacioRuta);
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mostrarMensajeEnDebug("Verificando permisos...");
        switch (requestCode) {
            case PermisosUtilitario.PEDIR_PERMISO_CAMARA: {
                if (PermisosUtilitario.aceptoTodosLosPermisos(grantResults)) {
                    mostrarMensajeEnDebug("Iniciando Cámara");
                    abrirCamara();
                } else {
                    mostrarMensajeSimple("No puede realizar ninguna acción si no habilita los permisos solicitados");
                    setCargoData(true);
                    ocultarBarraProgresoIndeterminado(contenedorPantalla);
                }
                return;
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                CropImage.activity(fotoURI).start(getContext(), this);
            } else {
                btnAgregarFoto.setVisibility(View.VISIBLE);
                btnAgregarFoto.setEnabled(true);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            //mostrarMensajeEnDebug("Entro a tomar la imagen cortada");
            mostrarBarraProgresoIndeterminado(contenedorPantalla);
            mostrarMensajeLargo("Guardando imagen, espere un momento por favor.");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    Bitmap croppedPhoto = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), resultUri);
                    fotoCortadaImagen = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),
                            resultUri);
                    //imageView.setImageBitmap(fotoCortadaImagen);

                    ContextWrapper cw = new ContextWrapper(getContext());
                    File directory = cw.getDir("denuncia", Context.MODE_PRIVATE);
                    if (!directory.exists()) {
                        directory.mkdir();
                    }

                    fotoCortadaArchivo=new File(directory, "thumbnail.png");


                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(fotoCortadaArchivo);
                        //croppedPhoto.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        croppedPhoto.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.close();
                        mostrarMensajeEnDebug("Proceso de guardado completo.");
                        mostrarPantallaBloqueo("Guardando corte,\n espere un momento por favor...");
                        presenter.subirCorte(fotoCortadaArchivo);
                        btnAgregarFoto.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        mostrarMensajeEnDebug("SAVE_IMAGE " + e.getMessage() + e);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                mostrarMensajeEnDebug(error.toString());
            }
        }
    }

    public void limpiarVariables(){
        fotoURI=null;
        fotoCortadaURI=null;
        fotoArchivo=null;
        fotoCortadaArchivo=null;
        fotoCortadaImagen=null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            ocultarTecladoSiEsPosible();
            mostrarMensajeEnDebug("Tab Visible");
        }else{
            //mostrarMensajeEnDebug("Tab Oculto");
        }
    }


    public void setListener(OnDenunciaFotosListener listener) {
        this.listener = listener;
    }

    public interface OnDenunciaFotosListener {

        Void getFoto(String fotoId);

    }
}
