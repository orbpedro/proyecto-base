package pe.orbelite.apps.proyecto.mvp.presenters;

import java.io.File;
import java.lang.ref.WeakReference;

import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaArchivosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaArchivosContrato.Presenter;
import retrofit2.Retrofit;

public class NuevaDenunciaArchivosPresenter implements Presenter {

    private WeakReference<NuevaDenunciaArchivosContrato.View> view;
    private Retrofit retrofit;

    public NuevaDenunciaArchivosPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void subirCorte(File file) {

    }

    @Override
    public void attachView(NuevaDenunciaArchivosContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view.clear();
    }
}
