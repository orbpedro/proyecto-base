package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaInformacionContrato;
import pe.orbelite.apps.proyecto.utilitarios.PermisosUtilitario;

public class NuevaDenunciaInformacionFragment extends BaseFragment implements NuevaDenunciaInformacionContrato.View {

    @Inject
    NuevaDenunciaInformacionContrato.Presenter presenter;

    private View root;
    private FrameLayout contenedorPantalla = null;

    private EditText edtPlaca;
    private EditText edtDescripcion;
    private TextView tvDireccion;
    private ImageView ivMapa;

    private double latitud;
    private double longitud;
    private String direccion;
    private String placa;
    private String descripcion;

    private OnNuevaDenunciaInformacionListener listener;

    public NuevaDenunciaInformacionFragment() {
        // Required empty public constructor
    }

    public static NuevaDenunciaInformacionFragment newInstance(OnNuevaDenunciaInformacionListener listener) {
        NuevaDenunciaInformacionFragment fragment = new NuevaDenunciaInformacionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_nueva_denuncia_informacion, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isCargoData()){
            presenter.iniciarGeoLocalizacion();
        }else{
            cargarDatos();
        }
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        edtPlaca = root.findViewById(R.id.edtPlaca);
        edtPlaca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                placa = edtPlaca.getText().toString();
                placa = placa.replace(" ", "");
                placa = placa.replace("-", "");
                placa = placa.toLowerCase();
                placa = placa.trim();

                listener.getInformacionData(placa,direccion,descripcion);
            }
        });

        edtDescripcion = root.findViewById(R.id.edtDescripcion);
        edtDescripcion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                descripcion = edtDescripcion.getText().toString();
                listener.getInformacionData(placa,direccion,descripcion);
            }
        });

        tvDireccion = root.findViewById(R.id.tvDireccion);
        ivMapa = root.findViewById(R.id.ivMapa);
    }


    private void cargarDatos() {
        mostrarMensajeEnDebug("Resuming.....");
        presenter.iniciarGeoLocalizacion();
    }

    @Override
    public boolean tieneAccesoGeo() {
        return tieneAccesoGeoVerificados();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mostrarMensajeEnDebug("Verificando permisos...");
        switch (requestCode) {
            case PermisosUtilitario.PEDIR_PERMISO_GEOLOCALIZACION: {
                if (PermisosUtilitario.aceptoTodosLosPermisos(grantResults)) {
                    mostrarMensajeEnDebug("Iniciando GEO");
                    presenter.iniciarGeoLocalizacion();
                } else {
                    mostrarMensajeSimple("No puede realizar ninguna acción si no habilita los permisos solicitados");
                    setCargoData(true);
                    ocultarBarraProgresoIndeterminado(contenedorPantalla);
                }
                return;
            }

        }
    }

    @Override
    public void mostrarRutaMapa(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;

        String urlMapa = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitud + "," + longitud
                + "&zoom=18&size=" + ivMapa.getWidth() + "x" + ivMapa.getHeight() + "&maptype=roadmap&markers=size:normal%7Ccolor:red%7Clabel:D%7C" + latitud + "%20," + longitud + "&sensor=false&scale=2&key=" + ProyectoApp.LLAVE_GOOGLE_API;

        mostrarMensajeEnDebug(urlMapa);

        Picasso.get().load(urlMapa).fit().centerCrop().into(ivMapa);
    }

    @Override
    public void mostrarLugar(String lugar) {
        direccion = lugar;
        tvDireccion.setText(lugar);
        listener.getInformacionData(placa,direccion,descripcion);
    }

    @Override
    public void errorAlConseguirLugar() {
        ocultarBarraProgresoIndeterminado(contenedorPantalla);
        direccion = "No se pudo obtener la dirección";
        tvDireccion.setText("No se pudo obtener la dirección");
        mostrarMensajeSimple("No se pudo obtener la dirección");
    }

    @Override
    public void errorAlConseguirDatosGeo() {
        mostrarMensajeSimple("No se pudo obtener coordenadas por favor intentar de nuevo más tarde");
    }

    @Override
    public void mostrarBarraEjecucion() {
        mostrarBarraProgresoIndeterminado(contenedorPantalla);
    }

    @Override
    public void ocultarBarraEjecucion() {
        ocultarBarraProgresoIndeterminado(contenedorPantalla);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            mostrarMensajeEnDebug("Tab Visible");
            if(isCargoData()){
                mostrarMensajeEnDebug("cargo data");
                if(tvDireccion.getText().toString()==null
                        || tvDireccion.getText().toString()==""){
                    //presenter.iniciarGeoLocalizacion();
                    //mostrarMensajeEnDebug("Buscando dirección");
                }
            }
        }else{
            //mostrarMensajeEnDebug("Tab Oculto");
            ocultarTecladoSiEsPosible();
        }
    }




    public void setListener(OnNuevaDenunciaInformacionListener listener) {
        this.listener = listener;
    }

    public interface OnNuevaDenunciaInformacionListener {

        Void getInformacionData(String placa,String direccion,String descripcion);

    }
}
