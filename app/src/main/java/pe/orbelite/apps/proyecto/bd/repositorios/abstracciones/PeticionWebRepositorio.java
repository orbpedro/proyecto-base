package pe.orbelite.apps.proyecto.bd.repositorios.abstracciones;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionWeb;

public interface PeticionWebRepositorio {

    void insertarOActualizar(PeticionWeb peticionWeb);

    PeticionWeb buscarPorUrl(String url);

    void eliminarPorUrl(String url);

    void eliminarTodo();
}
