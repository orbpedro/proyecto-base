package pe.orbelite.apps.proyecto.utilitarios;

import java.util.Date;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionWeb;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;

public class PeticionWebUtilitario {

    public static String obtenerJsonDePeticionWeb(PeticionWebRepositorio peticionWebRepositorio, String url) {
        PeticionWeb peticionWeb = peticionWebRepositorio.buscarPorUrl(url);

        if (peticionWeb == null) {
            return null;
        }

        Date now = new Date();

        long diff = now.getTime() - peticionWeb.getCreado().getTime();

        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        if (hours > 6) {
            return null;
        }

        return peticionWeb.getContenido();

    }

    public static void grabarContenidoPeticion(PeticionWebRepositorio peticionWebRepositorio, String url, String contenido) {

        PeticionWeb peticionWeb = peticionWebRepositorio.buscarPorUrl(url);

        if (peticionWeb == null) {
            peticionWeb = new PeticionWeb();
        }
        peticionWeb.setContenido(contenido);
        peticionWeb.setUrl(url);
        peticionWeb.setCreado(new Date());
        peticionWebRepositorio.insertarOActualizar(peticionWeb);
    }


}
