package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.mvp.contratos.NuevaDenunciaContrato;


public class NuevaDenunciaFragment extends PantallaFragment implements NuevaDenunciaContrato.View,
NuevaDenunciaInformacionFragment.OnNuevaDenunciaInformacionListener,
NuevaDenunciaAdicionalesFragment.OnNuevaDenunciaAdicionalesListener{

    private static final int MENU_GENERAR = 1111115;

    private View root;

    private String placa;
    private String direccion;
    private String descripcion;

    private String nota;

    @Inject
    NuevaDenunciaContrato.Presenter presenter;

    public NuevaDenunciaFragment() {
        // Required empty public constructor
    }

    public static NuevaDenunciaFragment newInstance() {
        NuevaDenunciaFragment fragment = new NuevaDenunciaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_nueva_denuncia, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.add(0, MENU_GENERAR, 3, getString(R.string.generar))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case MENU_GENERAR:
                mostrarMensajeEnDebug(placa+" "+direccion+" "+descripcion);
                mostrarMensajeEnDebug(nota);
                //cambiarFragment(DenunciaDetalleImprimirFragment.newInstance());
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.titulo_nueva_denuncia);
        setHasOptionsMenu(true);
        cargarDatos();
    }

    private void configurarControles() {
        ViewPager viewPager = root.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabs = root.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
    }

    private void cargarDatos() {

    }

    private void setupViewPager(ViewPager viewPager) {

        NuevaDenunciaAdapter adapter = new NuevaDenunciaAdapter(getChildFragmentManager());
        adapter.addFragment(NuevaDenunciaInformacionFragment.newInstance(this), "Informacion");
        adapter.addFragment(NuevaDenunciaInfraxionesFragment.newInstance(), "Infraxiones");
        adapter.addFragment(NuevaDenunciaArchivosFragment.newInstance(), "Fotos");
        adapter.addFragment(NuevaDenunciaAdicionalesFragment.newInstance(this), "Adicionales");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void miFuncion() {

    }

    static class NuevaDenunciaAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public NuevaDenunciaAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public Void getInformacionData(String placa, String direccion, String descripcion) {
        this.placa=placa;
        this.direccion=direccion;
        this.descripcion=descripcion;
        return null;
    }

    @Override
    public Void getAdicionalesData(String nota) {
        this.nota=nota;
        return null;
    }
}
