package pe.orbelite.apps.proyecto.mvp.contratos;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Papeleta;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface VehiculoDetallePapeletasContrato {

    interface  View extends BaseView {

        void mostrarPapeletas(List<Papeleta> papeletas);

    }

    interface  Presenter extends BasePresenter<View> {

        void listarPapeletas(String placa);
    }
}
