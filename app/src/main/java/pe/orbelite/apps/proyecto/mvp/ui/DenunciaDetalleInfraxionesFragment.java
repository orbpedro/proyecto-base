package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Infraccion;
import pe.orbelite.apps.proyecto.componentes.ui.base.BaseFragment;
import pe.orbelite.apps.proyecto.mvp.ui.dialogos.ListaDenunciaInfraccionesDialogFragment;


public class DenunciaDetalleInfraxionesFragment extends BaseFragment implements ListaDenunciaInfraccionesDialogFragment.OnDialogClickListener{

    private static final String ARG_INFRACCIONES = "ARG_INFRACCIONES";

    private LinearLayout contenedorInfraxiones;
    private View root;
    private Button btnAgregar;

    private List<Infraccion> infracciones;

    private HashMap<String, View> itemMap = new HashMap<>();
    private HashMap<String, Integer> itemIndex = new HashMap<>();

    private OnDenunciaInfraccionesListener listener;

    public DenunciaDetalleInfraxionesFragment() {
        // Required empty public constructor
    }

    public static DenunciaDetalleInfraxionesFragment newInstance(Serializable infracciones,
                                                                 OnDenunciaInfraccionesListener listener) {
        DenunciaDetalleInfraxionesFragment fragment = new DenunciaDetalleInfraxionesFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_INFRACCIONES, infracciones);
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            infracciones = (List<Infraccion>) getArguments().getSerializable(ARG_INFRACCIONES);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_denuncia_detalle_infraxiones, container, false);
        configurarControles();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ocultarTecladoSiEsPosible();
        cargarDatos();
    }

    private void configurarControles() {
        contenedorInfraxiones = root.findViewById(R.id.contenedorInfraxiones);
        btnAgregar = root.findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarInfraccion();
            }
        });
    }

    private void cargarDatos() {
        //contenedorInfraxiones.removeAllViews();
        //mostrarMensajeEnDebug("Cargar datos "+infracciones.size()+" infracciones");
        for(Infraccion infraccion:infracciones){
            mostrarInfracciones(infraccion);
        }
    }

    private void mostrarInfracciones(Infraccion infraccion){

        if(!itemMap.containsKey(infraccion.getCategoria())){
            View infraxionView = LayoutInflater.from(getActivity()).inflate(R.layout.view_denuncia_infraxion, null, false);
            TextView tvTitulo = infraxionView.findViewById(R.id.tvTitulo);
            TextView tvFecha = infraxionView.findViewById(R.id.tvFecha);
            TextView tvDescripcion = infraxionView.findViewById(R.id.tvDescripcion);

            if(infraccion.getFecha()==null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                tvFecha.setText(sdf.format(new Date()));
            }else{
                tvFecha.setText(infraccion.getFecha());
            }

            tvTitulo.setText(infraccion.getCategoria());
            tvDescripcion.setText(infraccion.getObservacion());

            contenedorInfraxiones.addView(infraxionView);

            itemIndex.put(infraccion.getCategoria(),itemIndex.size()+1);
            itemMap.put(infraccion.getCategoria(),infraxionView);
            mostrarMensajeSimple("Se agregó correctamente la infracción "+infraccion.getCategoria());
        }else{
            mostrarMensajeEnDebug("El item solicitado ya existe...");
        }

        mostrarMensajeEnDebug("Infracciones "+itemMap.size());
        listener.getInfracciones(infracciones);
    }


    public void agregarInfraccion(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        android.support.v4.app.Fragment prev = fragmentManager.findFragmentByTag("DIALOGO_SELECCIONAR_INFRACCION");
        if (prev != null) {
            ft.remove(prev);
        }

        ft.addToBackStack(null);
        ListaDenunciaInfraccionesDialogFragment fragment = ListaDenunciaInfraccionesDialogFragment.newInstance(itemIndex,this);
        fragment.show(ft, "DIALOGO_SELECCIONAR_INFRACCION");
    }

    public void setListener(OnDenunciaInfraccionesListener listener) {
        this.listener = listener;
    }

    public interface OnDenunciaInfraccionesListener {

        Void getInfracciones(List<Infraccion> infracciones);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
            mostrarMensajeEnDebug("Tab Visible");
            ocultarTecladoSiEsPosible();
        }else{
            //mostrarMensajeEnDebug("Tab Oculto");
        }
    }





    @Override
    public void onDialogInfraccionAgregarClick(String id, String descripcion) {
        Infraccion infraccion = new Infraccion();
        infraccion.setCategoria(id);
        infraccion.setObservacion(descripcion);

        infracciones.add(infraccion);
        mostrarInfracciones(infraccion);
    }

    @Override
    public void onDialogInfraccionQuitarClick(String id) {
        if(itemMap.containsKey(id)){
            //mostrarMensajeEnDebug("Hashmap antes: "+itemMap.size());
            //mostrarMensajeEnDebug("Removiendo "+id);

            View v = itemMap.get(id);
            contenedorInfraxiones.removeView(v);
            itemMap.remove(id);
            itemMap.remove(id);

            Infraccion infraccionEliminada = new Infraccion();
            for(int i = infracciones .size()-1 ; i >= 0; i--){
                if(infracciones .get(i).getCategoria()==id){
                    infracciones .remove(infracciones .get(i));
                    break;
                }
            }

            //mostrarMensajeEnDebug("Hashmap después: "+itemMap.size());
            mostrarMensajeSimple("Se removió correctamente la infracción "+id);
            mostrarMensajeEnDebug("Infracciones "+itemMap.size());

            listener.getInfracciones(infracciones);
        }
    }


}
