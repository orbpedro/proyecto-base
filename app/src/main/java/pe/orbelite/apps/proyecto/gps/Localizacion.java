package pe.orbelite.apps.proyecto.gps;
import android.location.Location;

public class Localizacion {

    public static final int SUCESO = 1;
    public static final int NO_SE_PUDO_ENCONTRAR_COORDENADA = 2;
    public static final int PLAY_SERVICES_NO_DISPONIBLE = 10;
    public static final int NO_SE_PUDO_CONECTAR_GOOGLE_API = 11;

    private Location location;
    private boolean suceso;
    private int codigo;
    private String mensaje;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isSuceso() {
        return suceso;
    }

    public void setSuceso(boolean suceso) {
        this.suceso = suceso;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
