package pe.orbelite.apps.proyecto.mvp.ui;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import pe.orbelite.apps.proyecto.R;


public class DenunciaDetalleAdicionalesFragment extends PantallaFragment {

    private EditText edtNota;

    private View root;
    private String nota;

    private OnAdicionalesListener listener;

    public DenunciaDetalleAdicionalesFragment() {
        // Required empty public constructor
    }


    public static DenunciaDetalleAdicionalesFragment newInstance(String Nota,
                                                                 OnAdicionalesListener listener) {
        DenunciaDetalleAdicionalesFragment fragment = new DenunciaDetalleAdicionalesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setListener(listener);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_denuncia_detalle_adicionales, container, false);
        configurarControles();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        ocultarTecladoSiEsPosible();
    }

    private void configurarControles() {
        edtNota = root.findViewById(R.id.edtNota);
        edtNota.addTextChangedListener((new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                listener.getNota(edtNota.getText().toString());
            }
        }));
    }

    public void setListener(OnAdicionalesListener listener) {
        this.listener = listener;
    }

    public interface OnAdicionalesListener {

        Void getNota(String adicional);

    }
}
