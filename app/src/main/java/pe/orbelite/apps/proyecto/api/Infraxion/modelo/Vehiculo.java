package pe.orbelite.apps.proyecto.api.Infraxion.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Vehiculo {

    @SerializedName("placa")
    @Expose
    private String placa;

    @SerializedName("marca")
    @Expose
    private String marca;

    @SerializedName("modelo")
    @Expose
    private String modelo;

    @SerializedName("color")
    @Expose
    private String color;

    @SerializedName("estado")
    @Expose
    private String estado;

    @SerializedName("totalPapeletas")
    @Expose
    private int totalPapeletas;

    @SerializedName("totalPagarPapeletas")
    @Expose
    private double totalPagarPapeletas;

    @SerializedName("soat")
    @Expose
    private Soat soat;

    @SerializedName("papeletas")
    @Expose
    private List<Papeleta> papeletas;


    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getTotalPapeletas() {
        return totalPapeletas;
    }

    public void setTotalPapeletas(int totalPapeletas) {
        this.totalPapeletas = totalPapeletas;
    }

    public double getTotalPagarPapeletas() {
        return totalPagarPapeletas;
    }

    public void setTotalPagarPapeletas(double totalPagarPapeletas) {
        this.totalPagarPapeletas = totalPagarPapeletas;
    }

    public Soat getSoat() {
        return soat;
    }

    public void setSoat(Soat soat) {
        this.soat = soat;
    }

    public List<Papeleta> getPapeletas() {
        return papeletas;
    }

    public void setPapeletas(List<Papeleta> papeletas) {
        this.papeletas = papeletas;
    }
}
