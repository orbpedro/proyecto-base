package pe.orbelite.apps.proyecto.api.Infraxion.servicios;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaCercanaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PapeletasPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Peticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface DenunciaService {

    @POST("api/denunciaVehicular/obtener_mis_denuncias")
    Call<List<Denuncia>> listarMisDenuncias(@Body Peticion peticion);

    @POST("api/denunciaVehicular/obtener_detalle_denuncia")
    Call<Denuncia> getDenunciaDetalle(@Body DenunciaPeticion peticion);

    @POST("api/denunciaVehicular/obtener_denuncias_cercanas")
    Call<List<Denuncia>> getDenunciaCercana(@Body DenunciaCercanaPeticion peticion);

    @POST("api/denunciaVehicular/gestionar_denuncia")
    Call<PeticionResult> putDenunciaGestionar(@Body DenunciaPeticion peticion);

    @POST("api/denunciaVehicular/generar_denuncia")
    Call<PeticionResult> putDenunciaGenerar(@Body Denuncia denuncia);

    @POST("api/denunciaVehicular/imprimir_denuncia")
    Call<PeticionResult> putDenunciaImprimir(@Body DenunciaPeticion peticion);

    @POST("api/denunciaVehicular/nueva_denuncia_policia")
    Call<Denuncia> postDenunciaPolicial(@Body Denuncia denuncia);
}
