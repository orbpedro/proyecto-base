package pe.orbelite.apps.proyecto.mvp.contratos;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Foto;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Infraccion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Vehiculo;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface DenunciaDetalleContrato {

    interface View extends BaseView {
        void mostrarDenunciaDetalle(Denuncia denuncia);
        void noDenunciaDetalle();
        void generarDenuncia(List<Infraccion> infracciones, List<Foto> fotos,String nota);
        void goDenunciaImprimir();
    }

    interface Presenter extends BasePresenter<DenunciaDetalleContrato.View> {
        void buscarDenunciaDetalle(String denunciaId);
        void guardarDenuncia(Denuncia denuncia);
    }

}
