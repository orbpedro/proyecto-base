package pe.orbelite.apps.proyecto.di.modulo;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.greenrobot.greendao.annotation.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import pe.orbelite.apps.proyecto.ProyectoApp;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


@Module
public class ApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(@NotNull final Context context) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder ongoing = chain.request().newBuilder();
                String token = obtenerTokenAcceso(context);

                if (token != null || token.trim().length() > 0) {
                    ongoing.addHeader("token-acceso", token);

                }

                return chain.proceed(ongoing.build());
            }
        };


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(interceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        return okHttpClient;

    }

    @Provides
    @Singleton
    @Named("generalRetrofit")
    Retrofit providesRetrofit(OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(ProyectoApp.DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @Named("textRetrofit")
    Retrofit providesRetrofitText(OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(ProyectoApp.DOMAIN)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @Named("apiMapRetrofit")
    Retrofit provideGoogleMapsApi(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    private String obtenerTokenAcceso(Context context) {
        SharedPreferences preferencias = context.getSharedPreferences(ProyectoApp.NOMBRE_PREFERENCIA, ProyectoApp.MODE_PRIVATE);

        return preferencias.getString(ProyectoApp.LLAVE_TOKEN_ACCESO, "");
    }
}
