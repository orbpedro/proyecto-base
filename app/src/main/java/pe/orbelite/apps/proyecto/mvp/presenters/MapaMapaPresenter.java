package pe.orbelite.apps.proyecto.mvp.presenters;

import com.google.android.gms.maps.model.Marker;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaCercanaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Peticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.PeticionResult;
import pe.orbelite.apps.proyecto.api.Infraxion.servicios.DenunciaService;
import pe.orbelite.apps.proyecto.bd.repositorios.abstracciones.PeticionWebRepositorio;
import pe.orbelite.apps.proyecto.gps.Localizacion;
import pe.orbelite.apps.proyecto.gps.LocalizacionCallBack;
import pe.orbelite.apps.proyecto.gps.Localizador;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaMapaContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.MapaMapaContrato.Presenter;
import pe.orbelite.apps.proyecto.utilitarios.FormatoUtilitario;
import pe.orbelite.apps.proyecto.utilitarios.PeticionWebUtilitario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MapaMapaPresenter implements Presenter, LocalizacionCallBack {

    private WeakReference<MapaMapaContrato.View> view;
    private Retrofit retrofit;
    private Localizador localizador;
    private PeticionWebRepositorio peticionWebRepositorio;

    @Inject
    public MapaMapaPresenter(Retrofit retrofit,PeticionWebRepositorio peticionWebRepositorio, Localizador localizador) {
        this.retrofit = retrofit;
        this.localizador = localizador;
        this.localizador.setCallback(this);
        this.peticionWebRepositorio = peticionWebRepositorio;
    }



    @Override
    public void localizacionTerminada(Localizacion localizacion) {
        if (view.get() == null) {
            return;
        }

        if (!localizacion.isSuceso() || localizacion.getLocation() == null) {
            view.get().mostrarMensajeNoHayCoordenada();
            return;
        }
        //view.get().mostrarMensajeEnDebug("Localizacion encontrada : " + localizacion.getLocation().getLatitude() + "," + localizacion.getLocation().getLongitude());
        view.get().moverMapaAUnLugar(localizacion.getLocation());

        getDenunciasCercanas(localizacion.getLocation().getLatitude(),localizacion.getLocation().getLongitude());
    }

    @Override
    public void iniciarGeoLocalizacion() {
        if (!view.get().tieneAccesoGeo()) {
            return;
        }
        localizador.cancelarLocalizacion();

        localizador.iniciarLocalizacion();
    }

    @Override
    public void getDenunciasCercanas(double miLatitud,double miLongitud) {
        view.get().mostrarMensajeEnDebug("Llamando al ws");

        /*
        if (leidoDeCache()) {
            return;
        }
        */

        DenunciaCercanaPeticion peticion = new DenunciaCercanaPeticion();
        peticion.setDistancia(4.0);
        peticion.setLatitud(miLatitud);
        peticion.setLongitud(miLongitud);

        Call<List<Denuncia>> call =   retrofit.create(DenunciaService.class).getDenunciaCercana(peticion);
        call.enqueue(new Callback<List<Denuncia>>() {
            @Override
            public void onResponse(Call<List<Denuncia>> call, Response<List<Denuncia>> response) {

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        view.get().mostrarMensajeEnDebug("Denuncias del WS: "+response.body().size());
                        PeticionWebUtilitario.grabarContenidoPeticion(peticionWebRepositorio, "denuncia/obtener_denuncias_cercanas", FormatoUtilitario.jsonDeObjeto(response.body()));
                        view.get().mostrarDenunciasCercanas(response.body());
                        break;
                    default:
                        view.get().accionGeneralEnErrorAccion();
                        break;
                }

            }

            @Override
            public void onFailure(Call<List<Denuncia>> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en llamada" + t.getMessage());
            }
        });

        //List<Denuncia> denuncias=null;
        //view.get().mostrarDenunciasCercanas(denuncias);
    }

    @Override
    public void putDenunciaGestionar(String id) {
        final String myId=id;
        DenunciaPeticion peticion = new DenunciaPeticion();
        peticion.setId(id);

        Call<PeticionResult> call = retrofit.create(DenunciaService.class).putDenunciaGestionar(peticion);
        call.enqueue(new Callback<PeticionResult>() {
            @Override
            public void onResponse(Call<PeticionResult> call, Response<PeticionResult> response) {
                view.get().mostrarMensajeEnDebug("WS res: "+response.code());

                if (view.get() == null) {
                    return;
                }

                switch (response.code()){
                    case 200:
                        view.get().mostrarMensajeEnDebug("Se marcó la denuncia como gestionada correctamente.");
                        view.get().onDialogVerClick(myId);
                        break;
                    default:
                        view.get().mostrarMensajeEnDebug("Hubo un problema al actualizar el estado, intente nuevamente por favor.");
                        view.get().mostrarMensajeEnDebug("Código de error: "+response.body().getErrorCod());
                        view.get().mostrarMensajeEnDebug("Errores"+response.body().getErrorLis());
                        break;
                }
            }

            @Override
            public void onFailure(Call<PeticionResult> call, Throwable t) {
                if (view.get() == null) {
                    return;
                }

                view.get().mostrarMensajeEnDebug("Error en peticion" + t.getMessage());
            }
        });
    }

    private boolean leidoDeCache() {

        if (view.get() == null) {
            return false;
        }

        String json = PeticionWebUtilitario.obtenerJsonDePeticionWeb(peticionWebRepositorio, "denuncia/obtener_denuncias_cercanas");
        List<Denuncia> denuncias = FormatoUtilitario.listaDeJson(json, Denuncia[].class);

        if (denuncias != null) {
            view.get().mostrarDenunciasCercanas(denuncias);
            return true;
        }

        return false;
    }

    @Override
    public void attachView(MapaMapaContrato.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view.clear();
    }
}
