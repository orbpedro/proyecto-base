package pe.orbelite.apps.proyecto.mvp.contratos;

import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface NuevaDenunciaContrato {

    interface View extends BaseView {
        void miFuncion();
    }

    interface Presenter extends BasePresenter<NuevaDenunciaContrato.View> {
        void miFuncion();
    }

}
