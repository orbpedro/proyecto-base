package pe.orbelite.apps.proyecto.mvp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import pe.orbelite.apps.proyecto.ProyectoApp;
import pe.orbelite.apps.proyecto.R;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.DenunciaPeticion;
import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Infraccion;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleFotosContrato;
import pe.orbelite.apps.proyecto.mvp.contratos.DenunciaDetalleImprimirContrato;

public class DenunciaDetalleImprimirFragment extends PantallaFragment implements DenunciaDetalleImprimirContrato.View {

    private static final String ARG_DENUNCIA_ID = "ARG_DENUNCIA_ID";
    private static final int MENU_IMPRIMIR = 1111113;

    @Inject
    DenunciaDetalleImprimirContrato.Presenter presenter;

    private FrameLayout contenedorPantalla;
    private View root;
    private TextView tvTitulo;
    private TextView tvFecha;
    private TextView tvDireccion;
    private ImageView ivFoto;
    private TextView tvPlaca;
    private TextView tvDescripcion;
    private LinearLayout contenedorInfraxiones;
    private TextView tvNota;

    private String denunciaId;

    private Denuncia denuncia;
    private Menu myMenu;

    public DenunciaDetalleImprimirFragment() {
        // Required empty public constructor
    }

    public static DenunciaDetalleImprimirFragment newInstance(String id) {
        DenunciaDetalleImprimirFragment fragment = new DenunciaDetalleImprimirFragment();
        Bundle args = new Bundle();
        //args.putSerializable(ARG_DENUNCIA_ID, id);
        args.putString(ARG_DENUNCIA_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            denunciaId = getArguments().getString(ARG_DENUNCIA_ID);
        }

        ProyectoApp.from(getContext()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the fragment_home for this fragment
        root = inflater.inflate(R.layout.fragment_denuncia_detalle_imprimir, container, false);
        configurarControles();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.myMenu = menu;
        this.myMenu.add(0, MENU_IMPRIMIR, 3, getString(R.string.imprimir))
                .setVisible(false)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        super.onCreateOptionsMenu(this.myMenu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_IMPRIMIR:
                mostrarMensajeEnDebug("IMPRIMIR...");
                presenter.putDenunciaImprimir();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.titulo_detalle_denuncia_imprimir);
        cargarDatos();
    }

    private void configurarControles() {
        contenedorPantalla = root.findViewById(R.id.contenedorPantalla);
        ivFoto = root.findViewById(R.id.ivFoto);
        tvTitulo = root.findViewById(R.id.tvTitulo);
        tvFecha = root.findViewById(R.id.tvFecha);
        tvDireccion = root.findViewById(R.id.tvDireccion);
        tvPlaca = root.findViewById(R.id.tvPlaca);
        tvDescripcion = root.findViewById(R.id.tvDescripcion);
        contenedorInfraxiones = root.findViewById(R.id.contenedorInfraxiones);
        tvNota = root.findViewById(R.id.tvNota);

    }

    private void cargarDatos() {
        mostrarBarraProgresoIndeterminado(contenedorPantalla);
        presenter.getDenuncia(denunciaId);

    }

    private void mostrarInfraxiones(List<Infraccion> infracciones) {
        contenedorInfraxiones.removeAllViews();

        for(Infraccion infraccion:infracciones){
            View infraxionView = LayoutInflater.from(getActivity()).inflate(R.layout.view_denuncia_infraxion, null, false);

            TextView tvTitulo = infraxionView.findViewById(R.id.tvTitulo);
            TextView tvFecha = infraxionView.findViewById(R.id.tvFecha);
            TextView tvDescripcion = infraxionView.findViewById(R.id.tvDescripcion);

            tvTitulo.setText(infraccion.getCategoria());
            tvFecha.setText(infraccion.getFecha());
            tvDescripcion.setText(infraccion.getObservacion());

            contenedorInfraxiones.addView(infraxionView);
        }
    }

    @Override
    public void mostrarDenunciaDatos(Denuncia denuncia) {
        tvTitulo.setText("Denuncia Urbana");
        tvFecha.setText(denuncia.getFecha());
        tvDireccion.setText(denuncia.getDireccion());
        Picasso.get().load(denuncia.getRutaFoto()).resize(80, 80).centerCrop().into(ivFoto);
        tvPlaca.setText(denuncia.getPlaca());
        tvDescripcion.setText(denuncia.getDescripcion());

        mostrarInfraxiones(denuncia.getInfracciones());

        tvNota.setText(denuncia.getNota());

        switch(denuncia.getEstado()){
            case "4":
                this.myMenu.findItem(MENU_IMPRIMIR).setVisible(true);
                break;
            case "5":
                this.myMenu.findItem(MENU_IMPRIMIR).setVisible(true);
                break;
            case "7":

                break;
        }

        ocultarBarraProgresoIndeterminado(contenedorPantalla);
    }

    @Override
    public void impresoOk() {
        mostrarMensajeLargo("Se marcó la denuncia como impresa.");
        this.myMenu.findItem(MENU_IMPRIMIR).setVisible(false);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        cambiarFragment(MapaFragment.newInstance());
                    }
                }, 1000);
    }

    @Override
    public void impresoKo() {
        mostrarMensajeLargo("Hubo un problema al actualizar el estado de la denuncia.");
    }

    @Override
    public void onBackPressed() {
        //cambiarFragment(DenunciaDetalleFragment.newInstance(denunciaId));
        //super.onBackPressed();
    }
}
