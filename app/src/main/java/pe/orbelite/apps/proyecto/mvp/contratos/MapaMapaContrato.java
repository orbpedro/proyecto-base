package pe.orbelite.apps.proyecto.mvp.contratos;

import android.location.Location;

import java.util.List;

import pe.orbelite.apps.proyecto.api.Infraxion.modelo.Denuncia;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BasePresenter;
import pe.orbelite.apps.proyecto.mvp.contratos.base.BaseView;

public interface MapaMapaContrato {

    interface View extends BaseView {
        boolean tieneAccesoGeo();
        void mostrarMensajeNoHayCoordenada();
        void moverMapaAUnLugar(Location location);
        void mostrarDenunciasCercanas(List<Denuncia> denuncias);
        void onDialogVerClick(String id);
    }

    interface Presenter extends BasePresenter<View> {
        void iniciarGeoLocalizacion();
        void getDenunciasCercanas(double miLatitud,double miLongitud);
        void putDenunciaGestionar(String id);
    }
}
