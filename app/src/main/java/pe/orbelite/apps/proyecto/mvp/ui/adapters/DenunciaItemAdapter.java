package pe.orbelite.apps.proyecto.mvp.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.orbelite.apps.proyecto.R;

public class DenunciaItemAdapter extends RecyclerView.Adapter<DenunciaItemAdapter.ViewHolder> {

    private List<DenunciaItem> datos;


    public DenunciaItemAdapter() {
        datos = new ArrayList<>();
    }

    public void agregarDatos(List<DenunciaItem> masDatos) {
        datos.clear();
        datos.addAll(masDatos);
    }

    public List<DenunciaItem> getDatos() {
        return datos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_lista_denuncias, parent, false);
        // set the view's size, margins, paddings and fragment_home parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DenunciaItem item = datos.get(position);
        holder.tvTitulo.setText(item.getTitulo() != null ? item.getTitulo() : "");
        holder.tvEstado.setText(item.getEstado() != null ? item.getEstado() : "");
        holder.tvDireccion.setText(item.getDireccion() != null ? item.getDireccion() : "");
        holder.tvFecha.setText(item.getFecha() != null ? item.getFecha() : "");
        holder.tvDescripcion.setText(item.getDescripcion() != null ? item.getDescripcion() : "");
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitulo;
        public TextView tvEstado;
        public TextView tvFecha;
        private TextView tvDireccion;
        private TextView tvDescripcion;

        public ViewHolder(View v) {
            super(v);
            tvTitulo = v.findViewById(R.id.tvTitulo);
            tvEstado = v.findViewById(R.id.tvEstado);
            tvFecha = v.findViewById(R.id.tvFecha);
            tvDireccion = v.findViewById(R.id.tvDireccion);
            tvDescripcion = v.findViewById(R.id.tvDescripcion);
        }
    }
}
